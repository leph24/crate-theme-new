<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crate
 */

	get_header(); 

	// get ID of the page
	$page_id = get_the_ID();
	if ( 'page' == get_option( 'show_on_front' ) ) {
		$page_id = get_option( 'page_for_posts' );
	}
?>


<?php
if ( have_posts() ) :

	if ( is_home() && ! is_front_page() ) : ?>
		<?php include(locate_template('partials/header.php')); ?>
	<?php
	endif; ?>

	<div class="cr-section-content">
		<div class="container">
    		<div class="row justify-content-center">
    			<div class="col">
    				<?php 
    					$count = 0;
    				?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php 
                            switch ($count % 6) {
                                case 0:
                                    $style = "cyan";
                                    break;
                                case 1:
                                    $style = "lightcyan";
                                    break;
                                case 2:
                                    $style = "grey";
                                    break;
                                case 3:
                                    $style = "cyan";
                                    break;
                                case 4:
                                    $style = "lightcyan";
                                    break;
                                case 5:
                                    $style = "grey";
                                    break;
                                default:
                                    # code...
                                    break;
                            }
                        ?>

						<?php 
						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'template-parts/content', get_post_format() );

						$count++; 

					endwhile; ?>

					<?php if ( function_exists('wp_bootstrap_pagination') ) wp_bootstrap_pagination(); ?>

					<?php else :
						get_template_part( 'template-parts/content', 'none' );
					endif; ?>
				</div>
			</div>
		</div>
	</div>

<?php
    // SECTIONS
    if (function_exists('set_acf_conf')) {
        $meta_data = set_acf_conf('sections', false, $page_id);
    }
?>

<?php if ( !empty($meta_data) ) : ?>
    <?php foreach ($meta_data as $key => $meta) : ?>
        <?php if ( is_array($meta) ) : ?>
            <?php foreach ($meta as $layout) : ?>
                <?php if (file_exists(get_template_directory() . '/partials/' . $layout['acf_fc_layout'] . '.php')) : ?>
                    <?php include(locate_template('partials/' . $layout['acf_fc_layout'] . '.php')); ?>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>



<?php
get_sidebar();
get_footer();
