<?php 
    // GET JSON FILE
    if (function_exists('set_acf_conf')) { 
      $meta_data = set_acf_conf('pricing'); 
    }

  $table = $meta_data['pricing_table'] ?? false;
?>

<?php if ( !empty($table) ): ?>
<div class="cr-section-content">
    <div class="container">
        <div class="row justify-content-center" style="margin-bottom:8rem;">
          <div class="col table-responsive">
            <?php echo $table; ?>
          </div>
        </div>
    </div>
</div>
<?php endif; ?>