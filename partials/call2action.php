<?php 
    $style = $layout['style'] ?? '';
    $title = $layout['title'] ?? '';
    $subtitle = $layout['subtitle'] ?? '';
    $description = $layout['description'] ?? '';

    $form = $layout['form'] ?? '';

    $url = $layout['link']['url'] ?? '';
    $target = $layout['link']['target'] ?? '';
    if (!empty($target)) {
        $target = 'target="'. $target .'" rel="noopener"';
    }

    $button = $layout['link']['title'] ?? '';
?>
<div class="cr-section-content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="cr-box <?php echo $style; ?> cr-cta">
                    <div class="row justify-content-center">
                        <div class="col-sm-10">
                            <?php if (!empty($subtitle)) : ?>
                                <div class="cr-box-tag">
                                    <?php echo $subtitle; ?>
                                    <div class="cr-seperator cr-seperator-thin cr-box-seperator"></div>
                                </div>
                            <?php endif; ?>

                            <div class="cr-box-body">
                                <?php if (!empty($title)) : ?>
                                    <h2><?php echo $title; ?></h2>
                                <?php endif; ?>

                                <?php if (!empty($description)) : ?>
                                    <div class="cr-box-text">
                                        <?php echo wpautop($description); ?>
                                    </div>
                                <?php endif; ?>

                                <?php if (!empty($form)) : ?>
                                    <?php echo $form; ?>
                                <?php endif; ?>

                                <?php if (!empty($url)) : ?>
                                    <div class="mt-5 text-center">
                                        <a href="<?php echo $url; ?>" <?php echo $target; ?> class="btn btn-primary btn-lg" title="$button"><?php echo $button; ?></a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>