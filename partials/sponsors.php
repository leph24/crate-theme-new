<?php 
    $title = $layout['title'] ?? '';
    $subtitle = $layout['subtitle'] ?? '';
    $description = $layout['description'] ?? '';

    $slides = $layout['slides'] ?? ''; 
?>

<?php if ( (count($slides) > 0) || ($title || $tag || $description) ) : ?>
    <div class="cr-section-content">
        <div class="container">
            <!-- BEGIN: Slider -->
            <!-- BEGIN: Single Header -->
            <?php if (
                $subtitle || $title || $description
            ) : ?>
                <div class="row justify-content-center">
                    <div class="cr-single-header  col  col-md-8">
                        <?php if ($subtitle) : ?>
                            <div class="cr-box-tag">
                                <?php echo $subtitle; ?>
                                <div class="cr-seperator cr-seperator-thin cr-box-seperator"></div>
                            </div>
                        <?php endif; ?>

                        <?php if ($title) : ?>
                            <h2><?php echo $title; ?></h2>
                        <?php endif; ?>

                        <?php if ($description) : ?>
                            <?php echo wpautop($description); ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (count($slides) > 0) : ?>
                <div class="row justify-content-center">
                    <div class="col">
                        <div class="photo-slider" data-slick='{"slidesToShow": 3, "slidesToScroll": 3, "dots": true}'>
                            <?php foreach ($slides as $key => $slide) : ?>
                                <?php 
                                    $imageId = $slide['image'] ?? '';
                                    $image = '';
                                    if (!empty($imageId)) {
                                        $image = wp_get_attachment_image( $imageId, 'slidernav' );
                                    }
                                    
                                    $subtitle = $slide['subtitle'] ?? '';
                                    $title = $slide['title'] ?? '';
                                    $description = $slide['description'] ?? '';
                                    $url = $slide['link']['url'] ?? '';
                                    $target = $slide['link']['target'] ?? '';
                                    if (!empty($target)) {
                                        $target = 'target="'. $target .'" rel="noopener"';
                                    }
                                ?>

                                <div>
                                    <?php if (!empty($url)) : ?>
                                        <a href="<?php echo $url; ?>" <?php echo $target; ?> title="<?php echo $title; ?>">
                                    <?php endif; ?>

                                    <?php echo $image; ?>

                                    <?php if ( !empty($title) ) : ?>
                                        <p>
                                            <?php echo $title;  ?>
                                        </p>
                                    <?php endif; ?>

                                    <?php if (!empty($url)) : ?>
                                        </a>
                                    <?php endif; ?>
                                </div>

                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <!-- END: Slider -->
        </div>
    </div>
<?php endif; ?>