<?php 
    $title = $layout['title'] ?? '';
    $size = $layout['size'] ?? false;
    $imageId = $layout['image'] ?? '';


    $image = $imageId ? wp_get_attachment_image( $imageId, 'image', false, ["class" => "figure-img img-fluid"] ) : '';
    if ($size) {
        $image = $imageId ? wp_get_attachment_image( $imageId, 'image-small', false, ["class" => "figure-img img-fluid"] ) : '';
    }
?>

<?php if ( !empty($image) ): ?>
<div class="cr-section-content">
    <div class="container">
        <div class="row justify-content-center">
              <div class="text-center  <?php echo $size ? 'col-md-8' : 'col' ; ?>">

                <figure class="figure">
                    <?php echo $image; ?>

                    <?php if ( !empty($title) ) : ?>
                        <figcaption class="text-center">
                            <?php echo $title; ?>
                        </figcaption>
                    <?php endif; ?>
                </figure>

            </div>
        </div>
    </div>
</div>
<?php endif; ?>