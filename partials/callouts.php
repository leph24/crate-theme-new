<?php 
    $title = $layout['title'] ?? false;
    $subtitle = $layout['subtitle'] ?? false;
    $description = $layout['description'] ?? false;

    // NARROW HEADER
    $narrowHeader = $layout['narrow_header'] ?? false;
    $headerClass = 'col-md-8';
    if ($narrowHeader) {
        $headerClass = 'col-md-6';
    }

    // SECTION LINE
    $sectionLine = '';
    if ( /*is_front_page() && */(!empty( $title) || !empty( $subtitle) || !empty( $description)) ) {
        $sectionLine = 'cr-section-single';
    }
?>

<div class="cr-section-content  <?php echo $sectionLine; ?>">
    <div class="container">

        <?php if (
            $subtitle || $title || $description
        ) : ?>
            <div class="row justify-content-center">
                <div class="cr-single-header  col  <?php echo $headerClass; ?>">
                    <?php if ($subtitle) : ?>
                        <div class="cr-box-tag">
                            <?php echo $subtitle; ?>
                            <div class="cr-seperator cr-seperator-thin cr-box-seperator"></div>
                        </div>
                    <?php endif; ?>

                    <?php if ($title) : ?>
                        <h2><?php echo $title; ?></h2>
                    <?php endif; ?>

                    <?php if ($description) : ?>
                        <?php echo wpautop($description); ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if (count($layout) > 0) : ?>
            <div class="row justify-content-center">

                <?php foreach ($layout as $key => $callouts) : ?>
                    <?php if ( is_array($callouts) ) : ?>
                        <?php 
                            // $column = 'col-8';
                            $column = ''; // not sure if column is used anymore
                            // if more than 2
                            if (count($callouts) > 2) {
                                $column = '';
                            }

                            if (count($callouts) > 3) {
                                $column = 'col-lg-3';
                            }
                        ?>

                        <?php foreach ($callouts as $key => $callout) : ?>

                            <?php //variables
                                $style = $callout['style'] ?? '';
                                $imageId = $callout['image'] ?? '';


                                // $image = wp_get_attachment_image( $imageId, 'callout', 'img-fluid' );

                                $img_src = wp_get_attachment_image_url( $imageId, 'callout' );
                                $img_srcset = wp_get_attachment_image_srcset( $imageId, 'callout' );
                                $img_alt = get_post_meta( $imageId, '_wp_attachment_image_alt', true ); 

                                $image = '<img data-src="'.esc_attr( $img_src ).'" data-srcset="'.esc_attr( $img_srcset ).'" sizes="(max-width: 520px) 100vw, 520px" class="img-fluid  photo" alt="'.$img_alt.'">';

                                $subtitle = $callout['subtitle'] ?? '';
                                $title = $callout['title'] ?? '';
                                $description = $callout['description'] ?? '';
                                $url = $callout['url']['url'] ?? '';
                                $button = $callout['url']['title'] ?? '';
                                    if (empty($button)) {
                                        $button = $url;
                                    }
                                $target = $callout['url']['target'] ?? '';
                                if (!empty($target)) {
                                    $target = 'target="'.$target.'" rel="noopener"';
                                }
                            ?>

                            <!-- LOOP -->
                            <div class="col-sm-6 col-md-4  cr-box <?php echo $column; ?> <?php echo $style; ?>">
                                <!-- IMAGE -->
                                <?php if (!empty($imageId)) : ?>
                                    <?php if ( !empty($url) ) {
                                        echo '<a class="cr-box-imgLink" href="'.$url.'" '.$target.'>';
                                    } ?>

                                        <?php echo $image; ?>
                                    
                                    <?php if ( !empty($url) ) {
                                        echo '</a>';
                                    } ?>

                                <?php endif; ?>

                                <div class="cr-box-body">
                                    <?php if( !empty($subtitle) ) : ?>
                                        <div class="cr-box-tag">
                                            <?php echo $subtitle; ?>
                                            <div class="cr-seperator cr-seperator-thin cr-box-seperator"></div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if( !empty($title) ) : ?>
                                        <h4 class="cr-box-title"><?php echo $title; ?></h4>
                                    <?php endif; ?>

                                    <?php if( !empty($description) ) : ?>
                                        <div class="cr-box-text"><?php echo wpautop($description); ?></div>
                                    <?php endif; ?>
                                </div>

                                <?php if( !empty($url) ) : ?>
                                    <div class="cr-box-footer">
                                        <div class="mt-5 mb-3 text-center">
                                            <a href="<?php echo $url; ?>" <?php echo $target; ?> class="btn btn-primary btn-lg" title="<?php echo $button; ?>"><?php echo $button; ?></a></div>
                                    </div>
                                <?php endif; ?>
                            </div>

                        <?php endforeach; ?>
                        
                    <?php endif; ?>
                <?php endforeach; ?>

            </div>
        <?php endif; ?>

    </div>
</div>