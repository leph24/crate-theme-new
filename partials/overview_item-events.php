<?php
    // GET JSON FILE
    if (function_exists('set_acf_conf')) { 
        $meta_post = set_acf_conf('events'); 
    }

    if (function_exists('set_acf_conf')) {
        $meta_data = set_acf_conf('header');
    }

    // IMAGE
    $image = get_the_post_thumbnail( get_the_ID(), 'blog' );
    if ( empty($image) ) {
        $image = '<img src="'.get_template_directory_uri().'/img/gfx_blog_default.png" alt="Blog Image">';
    }

    // DESCRIPTION
    $description = get_the_excerpt();
    if ( !empty($description) ) { 
        $description = explode("<!--more-->", $description)[0];
    }

    // EVENT DETAIL
    $startDate = $meta_post['start_date'] ?? '';
    $endDate = $meta_post['end_date'] ?? '';
    $venue = $meta_post['venue'] ?? '';


?>

<div class="cr-feature cr-feature--left">
    <div class="row cr-feature-body">
        <div class="col-md-4">
            <a href="" class="" target="<?php echo ''; ?>">
                <?php echo $image; ?>
            </a>
        </div>

        <div class="col">
            <p class="cr-feature-tag">
                <?php echo $venue; ?>
            </p>

            <h2>
                <?php echo get_the_title(); ?>
            </h2>

            <p class="cr-feature-mute">
                <?php echo $startDate; ?><br>
                <?php echo $endDate; ?>
            </p>

            <?php echo wpautop($description); ?>

            <a href="" target="<?php echo ''; ?>" class="cr-link-arrow" title="read more">Read more
                <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <polygon id="Line-2" points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
                </svg>
            </a>
        </div>
    </div>

    <div class="row cr-feature-footer">
        <div class="col-md-4"></div>
        <div class="col">
            <div class="cr-seperator cr-seperator-thin cr-seperator--left"></div>
        </div>
    </div>
</div>