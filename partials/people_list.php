<?php 
    $title = $layout['title'] ?? false;
    $subtitle = $layout['subtitle'] ?? false;
    $description = $layout['description'] ?? false;

    // NARROW HEADER
    $narrowHeader = $layout['narrow_header'] ?? false;
    $headerClass = 'col-md-8';
    if ($narrowHeader) {
        $headerClass = 'col-md-6';
    }

    // SECTION LINE
    $sectionLine = '';
    if ( /*is_front_page() && */(!empty( $title) || !empty( $subtitle) || !empty( $description)) ) {
        $sectionLine = 'cr-section-single';
    }
?>

<div class="cr-section-content  <?php echo $sectionLine; ?>">
    <div class="container">

        <?php if (
            $subtitle || $title || $description
        ) : ?>
            <div class="row justify-content-center">
                <div class="cr-single-header  col  <?php echo $headerClass; ?>">
                    <?php if ($subtitle) : ?>
                        <div class="cr-box-tag">
                            <?php echo $subtitle; ?>
                            <div class="cr-seperator cr-seperator-thin cr-box-seperator"></div>
                        </div>
                    <?php endif; ?>

                    <?php if ($title) : ?>
                        <h2><?php echo $title; ?></h2>
                    <?php endif; ?>

                    <?php if ($description) : ?>
                        <?php echo wpautop($description); ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if (count($layout) > 0) : ?>
            <div class="row justify-content-center">

                <?php foreach ($layout as $key => $lists) : ?>
                    <?php if ( is_array($lists) ) : ?>
                        <?php 
                            // $column = 'col-8';
                            $column = ''; // not sure if column is used anymore
                            // if more than 2
                            if (count($lists) > 2) {
                                $column = 'col-6 mb-5';
                            }

                            if (count($lists) > 3) {
                                $column = 'col-6 mb-5';
                            }
                        ?>

                        <?php foreach ($lists as $key => $list) : ?>

                            <?php //variables
                                $imageId = $list['image'] ?? '';
                                $image = wp_get_attachment_image( $imageId, 'slidercontent', 'img-fluid' );
                            ?>

                            <!-- LOOP -->
                            <div class="<?php echo $column; ?>">
                                <!-- IMAGE -->
                                <?php if (!empty($imageId)) : ?>
                                    <?php echo $image; ?>
                                <?php endif; ?>
                            </div>

                        <?php endforeach; ?>
                        
                    <?php endif; ?>
                <?php endforeach; ?>

            </div>
        <?php endif; ?>

    </div>
</div>