<?php 
    $style = $layout['style'] ?? false;
    $title = $layout['title'] ?? false;
    $subtitle = $layout['subtitle'] ?? false;
    $description = $layout['description'] ?? false;

    $featurelist = 'cr-feature-list col';
    $containerClass = '';

    $imageSize = 'header';

    switch ($style) {
        case 'style1':
            // image top
            $containerClass = 'cr-feature-list cr-feature-list--center col';
            $featurelist = 'featurelist_top';
            $imageSize = 'fl-top';
            break;
        case 'style2':
            // image left
            $containerClass = 'cr-feature-list cr-feature-list--left col';
            $featurelist = 'featurelist_left';
            $imageSize = 'slidernav';
            break;
        case 'style3':
            // list view
            $containerClass = 'cr-feature-list cr-feature-list--small col-md-8';
            $featurelist = 'featurelist_list';
            break;
        case 'style4':
            // alternating image left right
            $containerClass = 'cr-feature-list cr-feature-list--alternate col';
            $featurelist = 'featurelist_alternating';
            break;
        case 'style5':
            $containerClass = 'col';
            $featurelist = 'featurelist_fullwidth';
            break;
        default:
            # code...
            break;
    }

    $sectionLine = '';

    if ( /*is_front_page() &&*/ (!empty( $title) || !empty( $subtitle) || !empty( $description)) ) {
        $sectionLine = 'cr-section-single';
    }

    // NARROW HEADER
    $narrowHeader = $layout['narrow_header'] ?? false;
    $headerClass = 'col-md-8';
    if ($narrowHeader) {
        $headerClass = 'col-md-6';
    }

?>

<div class="cr-section-content <?php echo $sectionLine; ?>">
    <div class="container">

        <!-- SECTION -->
        <?php if (
            $subtitle || $title || $description
        ) : ?>
            <div class="row justify-content-center">
                <div class="cr-single-header <?php echo $headerClass; ?>">
                    <?php if ($subtitle) : ?>
                        <div class="cr-box-tag">
                            <?php echo $subtitle; ?>
                            <div class="cr-seperator cr-seperator-thin cr-box-seperator"></div>
                        </div>
                    <?php endif; ?>

                    <?php if ($title) : ?>
                        <h2><?php echo $title; ?></h2>
                    <?php endif; ?>

                    <?php if ($description) : ?>
                        <?php echo wpautop($description); ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>


            <!-- BLOCKS -->
        <div class="row justify-content-center">
            <?php foreach ($layout as $key => $features) : ?>
                <?php if ( is_array($features) ) : ?>

                    <div class="<?php echo $containerClass; ?>">
                        <?php foreach ($features as $key => $feature) : ?>
                            <?php
                                $imageId = $feature['image'] ?? '';
                                // $image = wp_get_attachment_image( $imageId, $imageSize, false, ["class" => "img-fluid"] );

                                $img_src = wp_get_attachment_image_url( $imageId, $imageSize );
                                $img_srcset = wp_get_attachment_image_srcset( $imageId, $imageSize );
                                $img_alt = get_post_meta( $imageId, '_wp_attachment_image_alt', true ); 

                                $image = '<img data-src="'.esc_attr( $img_src ).'" data-srcset="'.esc_attr( $img_srcset ).'" sizes="100vw" class="img-fluid  photo" alt="'.$img_alt.'">';


                                $title = $feature['title'] ?? '';
                                $subtitle = $feature['subtitle'] ?? '';
                                $description = $feature['description'] ?? '';

                                $description1 = $feature['description_1'] ?? false;

                                $description2 = $feature['description_2'] ?? false;
                                $description3 = $feature['description_3'] ?? false;

                                $url = $feature['url']['url'] ?? '';
                                $target = $feature['url']['target'] ?? '';
                                if (!empty($target)) {
                                    $target = 'target="'. $target .'" rel="noopener"';
                                }

                                $label = $feature['url']['title'] ?? '';
                                if (empty($label)) {
                                    $label = $url;
                                }

                                if (!empty($url) ) {
                                    if (!empty($image)) {
                                        $image = '<a href="'.$url.'" '.$target.'>' . $image . '</a>';
                                    }
                                    
                                    $title = '<a href="'.$url.'" '.$target.' title="'.$title.'">' . $title . '</a>';
                                }
                            ?>

                            <?php if (file_exists(get_template_directory() . '/partials/featurelist/'.$featurelist.'.php')) {
                                    include(locate_template('partials/featurelist/'. $featurelist .'.php')); 
                                }
                            ?>

                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <!-- END BLOCKS -->

    </div>
</div>