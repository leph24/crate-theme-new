<?php 
  $trial = $_GET['license'] ?? '';

  $expire = '';
  switch ($trial) {
    case 'expired':
      $expire = 'has expired';
      break;
    default:
      $expire = 'will expire soon';
      break;
  }
?>

<div class="cr-section-content">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <h3>Your CrateDB license <?php echo $expire; ?>.</h3>
        <p>Your options include:</p>
        <ul>
          <li>Purchasing a CrateDB license</li>
          <li>Requesting an trial extension</li>
          <li>Downgrading CrateDB to CrateDB Community</li>
        </ul>
      </div>
    </div>
  </div>
</div>