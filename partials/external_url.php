<?php 
    if (function_exists('set_acf_conf')) {
        $externalUrlData = set_acf_conf('external');

        if ( count($externalUrlData) > 0 ) {
            $externalUrl = $externalUrlData['external']['url'] ?? '';

            if ( !empty( $externalUrl ) ) {
                wp_redirect( $externalUrl );
                exit;
            }
        }
    }
?>