<?php 
    $quote = $layout['quote'] ?? '';
    $cite = $layout['cite'] ?? '';
    $company = $layout['company'] ?? '';
    $position = $layout['position'] ?? '';
?>

<?php if ( !empty($quote) ) : ?>
<div class="cr-section-content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <blockquote class="cr-quote  ">
                    <div class="cr-quote-text">
                        <?php echo wpautop($quote); ?>
                    </div>

                    <cite>
                        <?php echo $cite; ?>
                        <p class="text-muted">
                            <?php echo $position; ?>

                            <?php if (!empty($position) && !empty($company)) {echo '<br>';} ?>
                            <?php echo $company; ?>
                        </p>
                    </cite>
                </blockquote>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>