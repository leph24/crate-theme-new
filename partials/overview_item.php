<?php
    // GET JSON FILE
    if (function_exists('set_acf_conf')) { 
        $meta_post = set_acf_conf('post'); 
    }

    if (function_exists('set_acf_conf')) {
        $meta_data = set_acf_conf('header');
    }

    if (function_exists('set_acf_conf')) {
        $externalUrlData = set_acf_conf('external');
    }

    $title = get_the_title();

    $lead = $meta_data['lead'] ?? '';

    $image = get_the_post_thumbnail( get_the_ID(), 'blog' );
    if ( empty($image) ) {
        $image = '<img src="'.get_template_directory_uri().'/img/gfx_blog_default.png" alt="Blog Image">';
    }

    $description = get_the_excerpt();
    if ( !empty($description) ) { 
        $description = explode("<!--more-->", $description)[0];
    }

    // LINK to detail
    $externalUrl = $externalUrlData['external']['url'] ?? '';
    $externalTarget = $externalUrlData['external']['target'] ?? '';
    if (!empty($externalTarget)) {
        $externalTarget = 'target="'. $externalTarget .'" rel="noopener"';
    }

    $url = esc_url( get_permalink() );
    if (!empty($externalUrl)) {
        $url = $externalUrl;
        if (empty($externalTarget)) {
            $externalTarget = 'target="_blank"';
        }
    }

    // IF PRESS
    if (get_post_type() == 'press') {
        if (function_exists('set_acf_conf')) { 
            $meta_header = set_acf_conf('header'); 
            $meta_press = set_acf_conf('press'); 
        }

        $subtitle = $meta_press['source'] ?? '';

        if ( empty($description) ) {
            $description = $meta_header['lead'] ;
        }
    }

    if (function_exists('set_acf_conf')) {
        $overview_data = set_acf_conf('overview');
    }

    if (empty($overview_type)) {
        $overview_type = 'post';
    }

    // webinar & white_papers
    $button = 'read more';
    if ($overview_type == 'white_papers') {
        $button = 'Download';
    }

    if ( ($overview_type == 'webinars') || ($overview_type == 'videos') ) {
        if ( !empty($image) ) {
            $image = get_the_post_thumbnail( get_the_ID(), 'callout' );
        }
        $button = 'Watch now';
    }

    $target = '';
?>

<?php if ($overview_type == 'post' || $overview_type == 'press') : ?>
    <div class="cr-feature cr-feature--left">
        <div class="row cr-feature-body">
            <div class="col-md-4">
                <a href="<?php echo $url; ?>" class="" <?php echo $externalTarget; ?>>
                    <?php echo $image; ?>
                </a>
            </div>

            <div class="col">
                <p class="cr-feature-tag">
                    <?php if (get_post_type() == 'press') : ?>
                        <?php echo $subtitle; ?>
                    <?php else : ?>
                        <?php the_category( ' ', 'multiple', get_the_ID() ); ?>
                    <?php endif; ?>
                </p>

                <h2>
                    <a href="<?php echo $url; ?>" <?php echo $externalTarget; ?> title="<?php echo get_the_title(); ?>">
                        <?php echo get_the_title(); ?>
                    </a>
                </h2>

                <p class="cr-feature-mute">
                    <?php echo get_the_date(); ?>
                </p>

                <?php echo wpautop($description); ?>

                <a href="<?php echo $url; ?>" <?php echo $externalTarget; ?> class="cr-link-arrow">Read more
                    <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <polygon points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
                    </svg>
                </a>
            </div>
        </div>

        <div class="row cr-feature-footer">
            <div class="col-md-4"></div>
            <div class="col">
                <div class="cr-seperator cr-seperator-thin cr-seperator--left"></div>
            </div>
        </div>
    </div>
<?php else : ?>

    <div class="col-sm-6 col-md-4 col-lg-3 cr-box <?php echo $style; ?>">
        <!-- IMAGE -->
        <?php if (!empty($imageId)) : ?>
            <?php if ( !empty($url) ) {
                echo '<a class="cr-box-imgLink" href="'.$url.'" '.$target.'>';
            } ?>

                <?php echo $image; ?>
            
            <?php if ( !empty($url) ) {
                echo '</a>';
            } ?>

        <?php endif; ?>

        <div class="cr-box-body">
            <?php if( !empty($subtitle) ) : ?>
                <div class="cr-box-tag">
                    <?php echo $subtitle; ?>
                    <div class="cr-seperator cr-seperator-thin cr-box-seperator"></div>
                </div>
            <?php endif; ?>

            <?php if( !empty($title) ) : ?>
                <h4 class="cr-box-title"><?php echo $title; ?></h4>
            <?php endif; ?>

            <?php if( !empty($description) ) : ?>
                <div class="cr-box-text"><?php echo wpautop($description); ?></div>
            <?php endif; ?>
        </div>

        <?php if( !empty($url) ) : ?>
            <div class="cr-box-footer">
                <div class="mt-5 mb-3 text-center">
                    <a href="<?php echo $url; ?>" <?php echo $target; ?> class="btn btn-primary btn-lg" title="<?php echo $button; ?>"><?php echo $button; ?></a></div>
            </div>
        <?php endif; ?>
    </div>

<?php endif; ?>