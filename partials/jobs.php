<?php $title = $layout['title'] ?? false; ?>

<div class="cr-section-content cr-section-single">
    <div class="container">

        <div class="row justify-content-center">
          <div class="cr-single-header col-md-8">
            <h2><?php echo $title; ?></h2>
          </div>
        </div>

        <div class="row justify-content-center" id="grnhse_app">

        </div>

    </div>
</div>