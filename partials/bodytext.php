<?php 

    $title = $layout['title'] ?? false;
    $subtitle = $layout['subtitle'] ?? false;
    $description = $layout['description'] ?? false;
    $colClass = $layout['full_width'] ?? false;
    $fontsize = $layout['text_size'] ?? false;

    switch ($fontsize) {
        case 'bigger':
            $fontsize = 'style="font-size:1.5rem";';
            break;
        case 'biggest':
            $fontsize = 'style="font-size:1.75rem";';
            break;
        default:
            # code...
            break;
    }


    // BUTTON 1
    $link = $layout['link'] ?? false;
    $url =  $link['url'] ?? false;
    $target = $link['target'] ?? false;
    if (!empty($target)) {
        $target = 'target="'. $target .'" rel="noopener"';
    }
    $buttontext = $link['title'] ?? false;

    // Width
    if ($colClass) {
        $colClass = 'col';
    } else {
        $colClass = 'col-md-8';
    }
?>

<?php if ($description) : ?>
    <div class="cr-section-content">
        <div class="container">

            <?php if ($title || $subtitle) : ?>
                <div class="row justify-content-center">
                  <div class="cr-single-header col-md-8">
                    <div class="cr-box-tag">
                    <?php if ($subtitle) : ?>
                        <div><?php echo $subtitle; ?></div>
                        <div class="cr-seperator cr-seperator-thin cr-box-seperator"></div>
                    <?php endif; ?>
                   <?php if ($title) : ?>
                        <h2><?php echo $title; ?></h2>
                    
                  <?php endif; ?>
                </div>

                  </div>
                </div>
            <?php endif; ?>
            
            <div class="row justify-content-center">
                
                <div class="<?php echo $colClass; ?>">

                    <div class="table-responsive" <?php echo $fontsize; ?>>
                        <?php echo wpautop(do_shortcode($description)); ?>
                    </div>

                    <?php if ($link) : ?>
                        <div class="text-center">
                          <a href="<?php echo $url; ?>" <?php echo $target; ?> class="btn btn-primary btn-lg" title="'.$buttontext.'"><?php echo $buttontext; ?></a>
                        </div>
                    <?php endif; ?>
                </div>


            </div>
        </div>
    </div>
<?php endif; ?>