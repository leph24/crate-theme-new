<?php 
    $title = $layout['title'] ?? '';
    $subtitle = $layout['subtitle'] ?? '';
    $description = $layout['description'] ?? '';

    $gallery = $layout['gallery'] ?? ''; 
?>

<?php if ( (count($gallery) > 0) || ($title || $tag || $description) ) : ?>
    <div class="cr-section-content">
        <div class="container">
            <!-- BEGIN: Slider -->
            <!-- BEGIN: Single Header -->
            <?php if (
                $subtitle || $title || $description
            ) : ?>
                <div class="row justify-content-center">
                    <div class="cr-single-header  col  col-md-8">
                        <?php if ($subtitle) : ?>
                            <div class="cr-box-tag">
                                <?php echo $subtitle; ?>
                                <div class="cr-seperator cr-seperator-thin cr-box-seperator"></div>
                            </div>
                        <?php endif; ?>

                        <?php if ($title) : ?>
                            <h2><?php echo $title; ?></h2>
                        <?php endif; ?>

                        <?php if ($description) : ?>
                            <?php echo wpautop($description); ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (count($gallery) > 0) : ?>
                <div class="row justify-content-center">
                    <div class="col">
                        <div class="photo-slider" data-slick='{"slidesToShow": 3, "slidesToScroll": 3, "dots": true}'>
                            <!-- END: Single Header -->
                            <?php foreach ($gallery as $key => $imageId) : ?>
                                <?php $imageDescription = get_post($imageId)->post_content ?? ''; ?>
                                <div>
                                    <?php echo $image = $imageId ? wp_get_attachment_image( $imageId, 'callout', ["class" => ""] ) : ''; ?>
                                        <?php if ( !empty($imageDescription) ) : ?>
                                            <p>
                                                <?php echo $imageDescription;  ?>
                                            </p>
                                        <?php endif; ?>
                                    
                                </div>


                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <!-- END: Slider -->
        </div>
    </div>
<?php endif; ?>