<?php
    // SET VARIABLES
    $description = get_the_content();
?>

<?php if (!empty($description)) : ?>
<div class="cr-section-content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <?php echo wpautop($description); ?>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>