<?php 
    $colClass = '';

    if (!empty($description1) ) {
        $colClass = "col";
    }

    if (!empty($description2) && !empty($description1)) {
        $colClass = "col-md-4";
    }

    if (!empty($description1) && !empty($description3) && !empty($description2)) {
        $colClass = "col-md-4";
    }
?>

<div class="cr-feature cr-feature--left">
    <div class="row cr-feature-body">

        <div class="col-md-4">
            <!-- IMAGE -->
            <?php echo $image; ?>
        </div>

        <div class="col">
            <?php if (!empty($subtitle) ) : ?>
                <p class="cr-feature-tag"><?php echo $subtitle; ?></p>
            <?php endif; ?>
            

            <?php if (!empty($title) ) : ?>
                <h2><?php echo $title; ?></h2>
            <?php endif; ?>

            <div class="row">
                <div class="col">
                    <?php if (!empty($description) ) : ?>
                        <?php echo wpautop($description); ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="row">
                <?php if (!empty($description1) ) : ?>
                    <div class="<?php echo $colClass; ?>">
                        <?php echo wpautop($description1); ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($description2) ) : ?>
                    <div class="<?php echo $colClass; ?>">
                        <?php echo wpautop($description2); ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($description3) ) : ?>
                    <div class="<?php echo $colClass; ?>">
                        <?php echo wpautop($description3); ?>
                    </div>
                <?php endif; ?>
            </div>

            <?php if (!empty($url) ) : ?>
            <a href="<?php echo $url; ?>" <?php echo $target; ?> class="cr-link-arrow" title="<?php echo $label; ?>"><?php echo $label; ?>

                <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <polygon points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
                </svg>
            </a>
            <?php endif; ?>
        </div>
    </div>

    <div class="row cr-feature-footer">
        <div class="col-md-4"></div>

        <div class="col">
            <div class="cr-seperator cr-seperator-thin cr-seperator--left"></div>
        </div>
    </div>
</div>