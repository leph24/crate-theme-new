<?php 
    $smallImages = $layout['small_images'] ?? false;
    $imageTopClass = '';
    if ($smallImages) {
        $imageTopClass = 'feature-image-top';
    }


?>

<!-- BEGIN: Block -->
<div class="cr-feature text-center">
    <div class="cr-feature-body">
        <div class="<?php echo $imageTopClass; ?>">
            <?php echo $image; ?>
        </div>

        <?php if (!empty($title) ) : ?>
            <h2><?php echo $title; ?></h2>
        <?php endif; ?>

        <?php if (!empty($description) ) : ?>
            <?php echo wpautop($description); ?>
        <?php endif; ?>

        <?php if (!empty($url) ) : ?>
        <a href="<?php echo $url; ?>" <?php echo $target; ?> class="cr-link-arrow" title="<?php echo $label; ?>"><?php echo $label; ?>
            <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <polygon points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
            </svg>
        </a>
        <?php endif; ?>
    </div>
    <div class="cr-feature-footer">
        <div class="cr-seperator cr-seperator-thin"></div>
    </div>
</div>
<!-- END: Block -->