<div class="cr-feature">
    <div class="cr-feature-body d-flex">
        <div class="pt-1 mr-4">
            <?php if (!empty($url) ) : ?>
                <a href="'.$url.'" <?php echo $target; ?>">
            <?php endif; ?>
            <svg width="34px" height="19px" viewBox="0 0 34 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <path transform="translate(-336.000000, -809.000000)" fill-rule="nonzero" fill="#55D4F5" d="M366.085786,819.414214 L336,819.414214 L336,817.414214 L366.085786,817.414214 L359.085786,810.414214 L360.5,809 L369.914214,818.414214 L369.207107,819.12132 L360.5,827.828427 L359.085786,826.414214 L366.085786,819.414214 Z"></path>
              </svg>
            <?php if (!empty($url) ) : ?>
                </a>
            <?php endif; ?>
        </div>
        <div>
            <?php if (!empty($title) ) : ?>
                <h6><?php echo $title; ?></h6>
            <?php endif; ?>

            <?php if (!empty($description) ) : ?>
                <?php echo wpautop($description); ?>
            <?php endif; ?>
        </div>
    </div>
</div>