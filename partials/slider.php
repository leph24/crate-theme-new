<?php 
    $title = $layout['title'] ?? false;
    $subtitle = $layout['subtitle'] ?? false;
    $description = $layout['description'] ?? false;

    $slides = $layout['slides'] ?? false;

    $slideNav = '';
    $slideContent = '';
?>

<div class="cr-section-content">
    <div class="container">

        <div class="row justify-content-center">
            <div class="cr-single-header col-md-8">
                <?php if ($subtitle) : ?>
                    <div class="cr-box-tag">
                        <?php echo $subtitle; ?>
                        <div class="cr-seperator cr-seperator-thin cr-box-seperator"></div>
                    </div>
                <?php endif; ?>

                <?php if ($title) : ?>
                    <h2><?php echo $title; ?></h2>
                <?php endif; ?>

                <?php if ($description) : ?>
                    <?php echo wpautop($description); ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col">
                <?php if ( !empty($slides) ) : ?>
                    <?php foreach ($slides as $key => $slide) : ?>
                        <?php 
                            $imageId = $slide['image'] ?? '';
                            $image = wp_get_attachment_image( $imageId, 'slidernav' );

                            $imageId2 = $slide['image2'] ?? '';
                            $image2 = wp_get_attachment_image( $imageId2, 'slidercontent' );

                            $video = $slide['video'] ?? '';

                            $subtitle = $slide['subtitle'] ?? '';
                            $title = $slide['title'] ?? '';
                            $description = $slide['description'] ?? '';
                            $url = $slide['link']['url'] ?? '';
                            $button = $slide['link']['title'] ?? '';
                                if (empty($button)) {
                                    $button = $url;
                                }
                            $target = $slide['link']['target'] ?? '';
                            if (!empty($target)) {
                                $target = 'target="'. $target .'" rel="noopener"';
                            }

                            if ( !empty($video) ) {
                                $image2 = '<div class="youtube"><iframe title="'.$title.'" width="560" height="315" src="https://www.youtube.com/embed/'.$video.'" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe></div>';
                            }

                            // quote
                            $quote = $slide['quote'] ?? false;
                            $cite = $slide['cite'] ?? false;
                            $position = $slide['position'] ?? false;
                            $company = $slide['company'] ?? false;
                        ?>

                        <?php 
                        $slideNav .= '
                            <!-- Slider Nav -->
                            <div>'. $image .'</div>
                        '; ?>

                        <?php 
                        $slideContent .= '
                            <!-- Slide 1 -->
                            <div>
                                <div class="row">
                                    <div class="col-md-6">';
                                        if ( !empty($image2) ) {
                                            $slideContent .= $image2;
                                        }

                        $slideContent .= '
                                    </div>
                                    <div class="col-md-6">';

                                        if ( !empty($title) ) {
                                            $slideContent .= '<h2>'.$title.'</h2>';
                                        }

                                        if ( !empty($description) ) {
                                            $slideContent .= wpautop($description);
                                        }

                                        if ( !empty($quote) || !empty($position) || !empty($cite) || !empty($company) ) {
                                            $slideContent .= '
                                            <div class="cr-quote">
                                                '
                                            ;

                                            $slideContent .= '
                                                <div class="cr-quote-text">
                                                    "' . $quote . '"
                                                </div>';

                                            $slideContent .= $cite;


                                            $slideContent .= '<p class="text-muted">';

                                                $slideContent .= $position . '<br>';

                                                $slideContent .= $company; 

                                            $slideContent .= '</p>
                                            </div>';
                                        }

                                        if ( !empty($url) ) {
                                            $slideContent .= '
                                                <a href="'.$url.'" class="btn btn-primary btn-lg" '.$target.' title="'.$button.'">'.$button.'</a>
                                            ';
                                        }

                        $slideContent .= '
                                    </div>
                                </div>
                            </div>
                        '; ?>

                    <?php endforeach; ?>

                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="slider-nav">
                                <?php echo $slideNav; ?>
                            </div>

                            <div class="slider-for">
                                <?php echo $slideContent; ?>
                            </div>
                        </div>
                    </div>

                <?php endif; ?>
            </div>
        </div>

    </div>
</div>