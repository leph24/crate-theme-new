<div class="row justify-content-center">
    <div class="col-12 col-lg-8">
        <?php if ( !empty($description) ) : ?>
            <!-- LEAD -->
            <?php if ( is_front_page() ) : ?>
                <h3><?php echo $description; ?></h3>
            <?php else : ?>
                <div class="lead">
                    <?php echo wpautop($description); ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>


        <?php if ( 
            ( 
                (is_home() == false)
                && (get_post_type() == 'post')
                || (get_post_type() == 'press')
            ) 
            && (!(is_archive())) 
        ) : ?>

            <div class="cr-tag-article"><?php echo the_date(); ?><?php if ( get_post_type() != 'press') : ?>, by <?php echo get_the_author(); ?></div><?php endif; ?>
        <?php else : ?>
            <?php if ( !($call2actionScript_big) ) : ?>
                <div class="cr-seperator"></div>
            <?php endif; ?>
        <?php endif; ?>



        <?php if ( !empty($url) || !empty($url2) ) : ?>
            <div class="mt-5 mb-3 text-center">
                <?php if ( !empty($url) ) : ?>
                    <!-- CALL 2 ACTION -->
                    <a href="<?php echo $url; ?>" <?php echo $target; ?> class="btn btn-primary btn-lg" >
                        <?php echo $label; ?>
                    </a>
                <?php endif; ?>

                <?php if ( !empty($url2) ) : ?>
                    <!-- CALL 2 ACTION -->
                    <a href="<?php echo $url2; ?>" <?php echo $target2; ?> class="btn btn-secondary btn-lg" title="<?php echo $label2; ?>">
                        <?php echo $label2; ?>
                    </a>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</div>