<?php 
    $arrContextOptions = array(
      "ssl" => array(
        "verify_peer" => false,
        "verify_peer_name" => false,
      )
    );
    $url = "https://crate.io/releases.json";
    $json = file_get_contents($url, false, stream_context_create($arrContextOptions));
    $json_data = json_decode($json, true);



    $stableVersion = $json_data['stable']['version'] ?? '';
    $stableDate = $json_data['stable']['date'] ?? '';
    $stable = $json_data['stable']['downloads']['tar.gz']['url'] ?? '';
    $stableAsc = $json_data['stable']['downloads']['tar.gz']['asc'] ?? '';

    $testingDate = $json_data['testing']['date'] ?? '';
    $testingVersion = $json_data['testing']['version'] ?? '';
    $testing = $json_data['testing']['downloads']['tar.gz']['url'] ?? '';
    $testingAsc = $json_data['testing']['downloads']['tar.gz']['asc'] ?? '';

    $nightly = $json_data['nightly']['downloads']['tar.gz']['url'] ?? '';
    $nightlyVersion = $json_data['nightly']['version'] ?? '';
  // header_getcrate

  // SECTIONS
  if (function_exists('set_acf_conf')) {
      $tab_data = set_acf_conf('header_getcrate');
  }

    $title_cratedb = $tab_data['title_cratedb'] ?? '';
    $items_cratedb = $tab_data['repeater_cratedb'] ?? '';

    $title_quickstart = $tab_data['title_quickstart'] ?? '';
    $items_quickstart = $tab_data['repeater_quickstart'] ?? '';

    $title_hosted = $tab_data['title_hosted'] ?? '';
    $items_hosted = $tab_data['repeater_hosted'] ?? '';

    $items_global = $tab_data['repeater_global'] ?? '';

    // SCRIPTS
    $script_stable = $tab_data['script_tarball'] ?? '';
    $script_nightly = $tab_data['script_nightly'] ?? '';
    $script_testing = $tab_data['script_testing'] ?? '';
    $script_debian = $tab_data['script_debian'] ?? '';
    $script_rpm = $tab_data['script_rpm'] ?? '';
    $script_docker = $tab_data['script_docker'] ?? '';
    $script_ubuntu = $tab_data['script_ubuntu'] ?? '';
    $script_quickstart = $tab_data['script_quickstart'] ?? '';
    $script_hosted = $tab_data['script_hosted'] ?? '';
?>

<div class="row">
    <div class="col cr-header-download">
      <nav>
        <div class="nav nav-tabs text-center justify-content-center" id="downloadOptions" role="tablist">
          <a class="nav-item nav-link active" id="nav-cratedb-tab" data-toggle="tab" href="#cratedb" role="tab" aria-controls="nav-cratedb" aria-selected="true" title="CrateDB">CrateDB</a>
          <a class="nav-item nav-link" id="nav-quickstart-tab" data-toggle="tab" href="#quickstart" role="tab" aria-controls="nav-quickstart" aria-selected="false" title="Quickstart">Quickstart</a>
          <a class="nav-item nav-link" id="nav-hosted-tab" data-toggle="tab" href="#cloud" role="tab" aria-controls="nav-hosted" aria-selected="false" title="CrateDB Cloud">CrateDB Cloud</a>
        <div>
      </div></div></nav>
      <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="cratedb" role="tabpanel" aria-labelledby="nav-cratedb-tab">
          <div class="row justify-content-center mb-4 mx-0">
            <div class="col-md-6 p-0">
              <h4><?php echo $title_cratedb; ?></h4>
              <?php if (is_array($items_cratedb)) : ?>
                <?php foreach($items_cratedb as $item) : ?>
                  <?php 
                    $title = $item['title'] ?? '';
                    $description = $item['description'] ?? '';
                  ?>

                  <!-- BEGIN: Feature small -->
                    <div class="d-flex">
                      <div class="pt-1 mr-4">
                        <svg width="34px" height="19px" viewBox="0 0 34 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <path transform="translate(-336.000000, -809.000000)" fill-rule="nonzero" fill="#55D4F5" d="M366.085786,819.414214 L336,819.414214 L336,817.414214 L366.085786,817.414214 L359.085786,810.414214 L360.5,809 L369.914214,818.414214 L369.207107,819.12132 L360.5,827.828427 L359.085786,826.414214 L366.085786,819.414214 Z"></path>
                        </svg>
                      </div>
                      <div>
                        <h6><?php echo $title; ?></h6>
                        <p>
                          <?php echo $description; ?>
                        </p>
                      </div>
                    </div>
                  <!-- END: Feature small -->
                <?php endforeach; ?>
              <?php endif; ?>
            </div>
            <div class="col-md-6 d-flex flex-column justify-content-between border border-dark p-4">
              <div class="row mb-3">
                <div class="col-sm-4 font-weight-bold">
                  Release
                </div>
                <div class="col">
                  <select class="form-control" id="parent_selection">
                    <option value="stable">Stable (<?php echo $stableVersion; ?>)</option>
                    <option value="testing">Testing (<?php echo $testingVersion; ?>)</option>
                    <option value="nightly">Nightly (<?php echo $nightlyVersion; ?>)</option>
                  </select>
                </div>
              </div>
              <div class="row mb-3">
                <div class="col-sm-4 font-weight-bold">
                  Package
                </div>
                <div class="col">
                  <select class="form-control" id="selectPackage">
                    <option value="nightly" class="js-select-nightly">Tarball .tar.gz</option>
                    <option value="testing" class="js-select-testing">Tarball .tar.gz</option>

                    <option value="stable" class="js-select-stable">Tarball .tar.gz</option>
                    <option value="debian">Debian .deb</option>
                    <option value="rpm">Red Hat .rpm</option>
                    <option value="docker">Docker</option>
                    <option value="ubuntu">Ubuntu</option>
                  </select>
                </div>
              </div>

              
              <ul class="list-unstyled flex-grow-1">

                <li><a href="https://crate.io/docs" class="cr-link-arrow" target="_blank" title="Getting Started">Getting Started
                    <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <polygon id="Line-2" points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
                    </svg>
                  </a></li>

                <li><a href="https://crate.io/docs/crate/reference/en/latest/appendices/release-notes/" class="cr-link-arrow" target="_blank" title="Release Notes">Release Notes
                    <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <polygon id="Line-2" points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
                    </svg>
                </a></li>

                <!-- Doc URL Stable -->
                <li class="js-download  js-download-stable js-download-debian  js-download-rpm  js-download-docker  js-download-ubuntu"><a href="https://crate.io/docs/crate/reference/en/<?php echo $stableVersion; ?>/" class="cr-link-arrow" target="_blank" title="Documentation">Documentation
                    <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <polygon id="Line-2" points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
                    </svg>
                </a></li>

                <!-- Doc URL Testing -->
                <li class="js-download  js-download-testing"><a href="https://crate.io/docs/crate/reference/en/<?php echo $testingVersion; ?>/" class="cr-link-arrow" target="_blank" title="Documentation">Documentation
                    <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <polygon id="Line-2" points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
                    </svg>
                </a></li>

                <!-- Doc URL Nightly -->
                <li class="js-download  js-download-nightly"><a href="https://crate.io/docs/crate/reference/en/latest/" class="cr-link-arrow" target="_blank" title="Documentation">Documentation
                    <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <polygon id="Line-2" points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
                    </svg>
                </a></li>

              </ul>
              

              <!-- Todo: Make a loop -->

              <div class="text-center border-top border-dark pt-4">
                <button type="button" class="btn btn-primary  js-download  js-download-stable" data-toggle="modal" data-target="#downloadModal_stable" title="Download">
                  Download
                </button>

                <button type="button" class="btn btn-primary  js-download  js-download-nightly" data-toggle="modal" data-target="#downloadModal_nightly" title="Download">
                  Download
                </button>

                <button type="button" class="btn btn-primary  js-download  js-download-testing" data-toggle="modal" data-target="#downloadModal_testing" title="Download">
                  Download
                </button>

                <button type="button" class="btn btn-primary  js-download  js-download-debian" data-toggle="modal" data-target="#downloadModal_debian" title="Download">
                  Download
                </button>

                <button type="button" class="btn btn-primary  js-download  js-download-rpm" data-toggle="modal" data-target="#downloadModal_rpm" title="Download">
                  Download
                </button>

                <button type="button" class="btn btn-primary  js-download  js-download-docker" data-toggle="modal" data-target="#downloadModal_docker" title="Download">
                  Download
                </button>

                <button type="button" class="btn btn-primary  js-download  js-download-ubuntu" data-toggle="modal" data-target="#downloadModal_ubuntu" title="Download">
                  Download
                </button>
              </div>
            </div>
          </div>
        </div>



        <div class="tab-pane fade" id="quickstart" role="tabpanel" aria-labelledby="nav-quickstart-tab">
          <div class="row justify-content-center mb-4 mx-0">
            <div class="col-md-6 p-0">
              <h4><?php echo $title_quickstart; ?></h4>
              
              <?php if ( is_array($items_quickstart) ) : ?>
                <?php foreach ($items_quickstart as $item) : ?>
                  <?php 
                    $title = $item['title'] ?? '';
                    $description = $item['description'] ?? '';
                  ?>

                  <!-- BEGIN: Feature small -->
                    <div class="d-flex">
                      <div class="pt-1 mr-4">
                        <svg width="34px" height="19px" viewBox="0 0 34 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <path transform="translate(-336.000000, -809.000000)" fill-rule="nonzero" fill="#55D4F5" d="M366.085786,819.414214 L336,819.414214 L336,817.414214 L366.085786,817.414214 L359.085786,810.414214 L360.5,809 L369.914214,818.414214 L369.207107,819.12132 L360.5,827.828427 L359.085786,826.414214 L366.085786,819.414214 Z"></path>
                        </svg>
                      </div>
                      <div>
                        <h6><?php echo $title; ?></h6>
                        <p>
                          <?php echo $description; ?>
                        </p>
                      </div>
                    </div>
                  <!-- END: Feature small -->
                <?php endforeach; ?>
              <?php endif; ?>
              
            </div>
            <div class="col-md-6 d-flex flex-column justify-content-between border border-dark p-4">
              <div class="row mb-3">
                <div class="col-sm-4 font-weight-bold">
                  Release
                </div>
                <div class="col">
                  Latest Stable (<?php echo $stableVersion; ?>)
                </div>
              </div>
              

              <ul class="list-unstyled flex-grow-1">

                <li><a href="https://crate.io/docs" class="cr-link-arrow" target="_blank" title="Getting Started">Getting Started
                    <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <polygon id="Line-2" points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
                    </svg>
                  </a></li>

                <li><a href="https://crate.io/docs/crate/reference/en/latest/appendices/release-notes/" class="cr-link-arrow" target="_blank" title="Release Notes">Release Notes
                    <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <polygon id="Line-2" points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
                    </svg>
                </a></li>

                <!-- Doc URL Stable -->
                <li class=""><a href="https://crate.io/docs/crate/reference/en/<?php echo $stableVersion; ?>/" class="cr-link-arrow" target="_blank" title="Documentation">Documentation
                    <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <polygon id="Line-2" points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
                    </svg>
                </a></li>
              </ul>

              <div class="text-center border-top border-dark pt-4">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#downloadModal_quickstart">
                  Download
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="cloud" role="tabpanel" aria-labelledby="nav-hosted-tab">
          <div class="row justify-content-center mb-4 mx-0">
            <div class="col-md-6 p-0">
              <h4><?php echo $title_hosted; ?></h4>
              
              <?php if ( is_array($items_hosted) ) : ?>
                <?php foreach ($items_hosted as $item) : ?>
                  <?php 
                    $title = $item['title'] ?? '';
                    $description = $item['description'] ?? '';
                  ?>

                  <!-- BEGIN: Feature small -->
                    <div class="d-flex">
                      <div class="pt-1 mr-4">
                        <svg width="34px" height="19px" viewBox="0 0 34 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <path transform="translate(-336.000000, -809.000000)" fill-rule="nonzero" fill="#55D4F5" d="M366.085786,819.414214 L336,819.414214 L336,817.414214 L366.085786,817.414214 L359.085786,810.414214 L360.5,809 L369.914214,818.414214 L369.207107,819.12132 L360.5,827.828427 L359.085786,826.414214 L366.085786,819.414214 Z"></path>
                        </svg>
                      </div>
                      <div>
                        <h6><?php echo $title; ?></h6>
                        <p>
                          <?php echo $description; ?>
                        </p>
                      </div>
                    </div>
                  <!-- END: Feature small -->
                <?php endforeach; ?>
              <?php endif; ?>
             
            </div>
            <div class="col-md-6 d-flex flex-column  border border-dark p-4">
              <h4>Request a CrateDB Cloud account</h4>


              <?php echo $script_hosted; ?>

              <div class="marketofallback">
                <a class="mktoButton" href="mailto:sales@crate.io" title="Contact">Contact</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>


<!-- TODO: Make a loop -->

<!-- BEGIN: Modal stable-->
<div class="modal fade" id="downloadModal_stable" tabindex="-1" role="dialog" aria-labelledby="downloadModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="downloadModalTitle">Download CrateDB</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <?php echo $script_stable; ?>

        <!-- FALLBACK -->
        <div class="marketofallback">
          <a class="mktoButton" href="<?php echo home_url(); ?>/download/thank-you/?download=tar" title="Download">Download</a>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- END: Modal -->

<!-- BEGIN: Modal NIGHTLY-->
<div class="modal fade" id="downloadModal_nightly" tabindex="-1" role="dialog" aria-labelledby="downloadModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="downloadModalTitle">Download CrateDB</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <?php echo $script_nightly; ?>

        <!-- FALLBACK -->
        <div class="marketofallback">
          <a class="mktoButton" href="<?php echo home_url(); ?>/download/thank-you/?download=tarNightly" title="Download">Download</a>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- END: Modal -->

<!-- BEGIN: Modal Testing-->
<div class="modal fade" id="downloadModal_testing" tabindex="-1" role="dialog" aria-labelledby="downloadModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="downloadModalTitle">Download CrateDB</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <?php echo $script_testing; ?>

        <!-- FALLBACK -->
        <div class="marketofallback">
          <a class="mktoButton" href="<?php echo home_url(); ?>/download/thank-you/?download=tarTest" title="download">Download</a>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- END: Modal -->

<!-- BEGIN: Modal DEBIAN-->
<div class="modal fade" id="downloadModal_debian" tabindex="-1" role="dialog" aria-labelledby="downloadModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="downloadModalTitle">Download CrateDB</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <?php echo $script_debian; ?>
        <!-- FALLBACK -->
        <div class="marketofallback">
          <a class="mktoButton" href="<?php echo home_url(); ?>/download/thank-you/?download=debian" title="download">Download</a>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- END: Modal -->


<!-- BEGIN: Modal RPM-->
<div class="modal fade" id="downloadModal_rpm" tabindex="-1" role="dialog" aria-labelledby="downloadModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="downloadModalTitle">Download CrateDB</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <?php echo $script_rpm; ?>

        <!-- FALLBACK -->
        <div class="marketofallback">
          <a class="mktoButton" href="<?php echo home_url(); ?>/download/thank-you/?download=rpm" title="download">Download</a>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- END: Modal -->


<!-- BEGIN: Modal Docker -->
<div class="modal fade" id="downloadModal_docker" tabindex="-1" role="dialog" aria-labelledby="downloadModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="downloadModalTitle">Download CrateDB</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <?php echo $script_docker; ?>

        <!-- FALLBACK -->
        <div class="marketofallback">
          <a class="mktoButton" href="<?php echo home_url(); ?>/download/thank-you/?download=docker" title="download">Download</a>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- END: Modal -->

<!-- BEGIN: Modal ubuntu -->
<div class="modal fade" id="downloadModal_ubuntu" tabindex="-1" role="dialog" aria-labelledby="downloadModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="downloadModalTitle">Download CrateDB</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <?php echo $script_ubuntu; ?>

        <!-- FALLBACK -->
        <div class="marketofallback">
          <a class="mktoButton" href="<?php echo home_url(); ?>/download/thank-you/?download=ubuntu" title="download">Download</a>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- END: Modal -->

<!-- BEGIN: Modal quickstart-->
<div class="modal fade" id="downloadModal_quickstart" tabindex="-1" role="dialog" aria-labelledby="downloadModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="downloadModalTitle">Download CrateDB</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

      </div>

      <div class="modal-body">
        <?php echo $script_quickstart; ?>

        <!-- FALLBACK -->
        <div class="marketofallback">
          <a class="mktoButton" href="<?php echo home_url(); ?>/download/thank-you/?download=quickstart" title="download">Download</a>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- END: Modal -->


