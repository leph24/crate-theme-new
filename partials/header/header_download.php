<?php 
    $arrContextOptions = array(
      "ssl" => array(
        "verify_peer" => false,
        "verify_peer_name" => false,
      )
    );
    $url = "https://crate.io/releases.json";
    $json = file_get_contents($url, false, stream_context_create($arrContextOptions));
    $json_data = json_decode($json, true);

    $stableVersion = $json_data['stable']['version'] ?? '';
    $stableDate = $json_data['stable']['date'] ?? '';
    $stable = $json_data['stable']['downloads']['tar.gz']['url'] ?? '';
    $stableAsc = $json_data['stable']['downloads']['tar.gz']['asc'] ?? '';

    $testingDate = $json_data['testing']['date'] ?? '';
    $testingVersion = $json_data['testing']['version'] ?? '';
    $testing = $json_data['testing']['downloads']['tar.gz']['url'] ?? '';
    $testingAsc = $json_data['testing']['downloads']['tar.gz']['asc'] ?? '';

    $nightly = $json_data['nightly']['downloads']['tar.gz']['url'] ?? '';
    $nightlyVersion = $json_data['nightly']['version'] ?? '';
?>







<!-- OLD DOWNLOAD -->
<div class="row">
    <div class="col cr-header-download">
        <nav>
            <div class="nav nav-tabs justify-content-center" id="downloadOptions" role="tablist">
              <a class="nav-item nav-link active" id="nav-quickstart-tab" data-toggle="tab" href="#quickstart" role="tab" aria-controls="nav-quickstart" aria-selected="true" title="Quickstart">Quickstart</a>
              <a class="nav-item nav-link" id="nav-stable-tab" data-toggle="tab" href="#stable" role="tab" aria-controls="nav-stable" aria-selected="false" title="Stable">Stable</a>
              <a class="nav-item nav-link" id="nav-testing-tab" data-toggle="tab" href="#testing" role="tab" aria-controls="nav-testing" aria-selected="false" title="Testing">Testing</a>
            <div>
          </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="quickstart" role="tabpanel" aria-labelledby="nav-quickstart-tab">
                <div class="row justify-content-center mb-4">
                    <div class="col-md-8">
                        The fastest way to try out CrateDB is by running:
                          <div class="d-flex">
                            <pre class="cr-pre-header mb-0 mr-2" style="width:100%"><code>$ </code><code class="language-bash mb-0" id="code1">bash -c "$(curl -L try.crate.io)"</code></pre>
                            <a href="#" class="btn btn-primary codebut" data-clipboard-target="#code1" title="Copy">Copy</a>
                          </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        Or spin up the official Docker image:
                      <div class="d-flex">
                        <pre class="cr-pre-header mb-0 mr-2" style="width:100%"><code>$ </code><code class="language-bash mb-0" id="code2">docker run -p 4200:4200 crate</code></pre>
                        <a href="#" class="btn btn-primary codebut" data-clipboard-target="#code2" title="Copy">Copy</a>
                      </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="stable" role="tabpanel" aria-labelledby="nav-stable-tab">
                <div class="row justify-content-center">
                    <div class="col-md-8 text-center">
                        <p>Download a tarball of the latest stable release of CrateDB.</p>
                        <p>Version: <?php echo $stableVersion; ?><br>
                        Release Date: <?php echo $stableDate; ?></p>
                        <a href="<?php echo $stable; ?>" class="btn btn-primary btn-lg mr-2 mb-2" title="Download .tar.gz">Download .tar.gz</a><br>
                        <a href="<?php echo $stableAsc; ?>" class="cr-link-arrow" title="Download  .asc">Download  .asc
                            <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <polygon points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="testing" role="tabpanel" aria-labelledby="nav-testing-tab">
                <div class="row justify-content-center">
                    <div class="col-md-8 text-center">
                        <p>The testing version of CrateDB gives you early access to the newest functionality.</p>
                        <p>Version: <?php echo $testingVersion; ?><br>
                        Release Date: <?php echo $testingDate; ?></p>
                        <a href="<?php echo $testing; ?>" class="btn btn-primary btn-lg mr-2 mb-2" title="Download .tar.gz">Download .tar.gz</a><br>
                        <a href="<?php echo $nightly; ?>" class="cr-link-arrow" title="Nightly build (<?php echo $nightlyVersion; ?>)">Nightly build (<?php echo $nightlyVersion; ?>)
                            <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <polygon points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>