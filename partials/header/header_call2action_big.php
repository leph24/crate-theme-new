<?php 
    // CALL 2 Action Top
    $call2actionTop = $meta_data['call_2_action_top'] ?? '';
    $call2actionTopLabel = $meta_data['call_2_action_top_button']['title'] ?? '';
    $call2actionTopTarget = $meta_data['call_2_action_top_button']['target'] ?? '';
    if (!empty($call2actionTopTarget)) {
        $call2actionTopTarget = 'target="'.$call2actionTopTarget.'" rel="noopener"';
    }

    $call2actionTopUrl = $meta_data['call_2_action_top_button']['url'] ?? '';
    if (empty($call2actionTopLabel)) {
        $call2actionTopLabel = $call2actionTopUrl;
    }

    $call2actionDescription = $meta_data['call_2_action_description'] ?? '';

    $call2actionScript = $meta_data['call_2_action_script'] ?? '';
?>



<div class="cr-section-content">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10">
        <div class="cr-box cyan cr-cta cr-cta-header">
          <div class="row justify-content-center">
            <div class="col-md-10">
              <div class="cr-box-body">
                <?php if (!empty($call2actionTop)) : ?>
                  <h2><?php echo $call2actionTop; ?></h2>
                <?php endif; ?>

                <?php if (!empty($call2actionDescription)) : ?>
                  <p class="cr-box-text"><?php echo $call2actionDescription; ?></p>
                <?php endif; ?>
                

                <?php echo $call2actionScript; ?>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>