<?php
    $args = array(
        'post_type'      => 'page',
        'posts_per_page' => -1,
        'post_parent'    => $post->ID,
        'order'          => 'ASC',
        'orderby'        => 'menu_order'
     );

    $parent = new WP_Query( $args ); 
?>

<?php if ( $parent->have_posts() ) : ?>


<div class="cr-nav-cloud">
    <div class="container">
        <ul class="list-inline justify-content-center">

            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
                <li class="list-inline-item cat-item">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" title="<?php echo get_the_title(); ?>"><?php the_title(); ?></a>
                </li>
            <?php endwhile; ?>

        </ul>

    </div>
</div>

<?php endif; ?>

<?php wp_reset_postdata(); ?>