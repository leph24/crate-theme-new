<?php 
    // CALL 2 Action Top
    $call2actionTop = $meta_data['call_2_action_top'] ?? '';
    $call2actionTopLabel = $meta_data['call_2_action_top_button']['title'] ?? '';
    $call2actionTopTarget = $meta_data['call_2_action_top_button']['target'] ?? '';
    if (!empty($call2actionTopTarget)) {
        $call2actionTopTarget = 'target="'.$call2actionTopTarget.'" rel="noopener"';
    }

    $call2actionTopUrl = $meta_data['call_2_action_top_button']['url'] ?? '';
    if (empty($call2actionTopLabel)) {
        $call2actionTopLabel = $call2actionTopUrl;
    }

    $call2actionScript = $meta_data['call_2_action_script'] ?? '';
?>

<?php if ( 
    !empty($call2actionTopUrl) 
    || !empty($call2actionTop) 
    || !empty( $call2actionScript )
) : ?>
    <!-- CALL 2 ACTION TOP -->
    <div class="cr-cta-top">
        <div class="container">
            <div class="d-md-flex justify-content-center align-items-center">
                <?php if (!empty($call2actionTop)) : ?>
                    <h2><?php echo $call2actionTop; ?></h2>
                <?php endif; ?>

                <?php if ( !empty($call2actionScript) ) : ?>
                    <?php echo $call2actionScript; ?>
                <?php elseif ( !empty($call2actionTopUrl) ) : ?>
                    <!-- CALL 2 ACTION TOP -->
                    <a href="<?php echo $call2actionTopUrl; ?>" <?php echo $call2actionTopTarget; ?> class="btn btn-secondary text-nowrap" title="<?php echo $call2actionTopLabel; ?>">
                        <?php echo $call2actionTopLabel; ?>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>

