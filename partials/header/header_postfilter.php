<?php 
// POST FILTER
if (
    ( 
        'page' == get_option('show_on_front')
        && get_post_type() == 'post'
        && is_single() == false
    )
    || (
        (
            get_page_template_slug( $post->ID ) === 'page-overview.php') 
            && (($overview_type == 'post') 
    //         || ($overview_type == 'white_papers')
    //         || ($overview_type == 'webinars')
        ) 
    )
) : 

    // all
    $allLink = '';


    switch ($overview_type) {
        case 'post':
            $allLink = 'blog';
            break;
        case 'white_papers':
            $allLink = 'recources/white-papers';
            break;
        case 'webinars':
            $allLink = 'recources/webinars';
            break;
        default:
            $allLink = 'blog';
            break;
    }
?>

    <!-- IF Overview Template and Blogposts -->
    <div class="cr-nav-cloud">
        <div class="container">
            <ul class="list-inline justify-content-center">

                <li class="list-inline-item cat-item  <?php echo $activeCat; ?>">
                    <a href="<?php echo esc_url( home_url( '/' ) ) . $allLink; ?>" class="general">
                        General
                    </a>
                </li>

            <?php wp_list_categories(array(
                'title_li' => '',
                'walker'   => new Walker_Category_Class(),
                'exclude'=> 1,
            )); ?> 
            </ul>
        </div>
    </div>
<?php endif; ?>