<?php 
    // PRESS FILTER
    if ( 
        ( is_archive() && get_post_type() == 'press' )
        || ( (get_page_template_slug( $post->ID ) === 'page-overview.php') && ($overview_type == 'press') )
    ) : ?>

    <?php $args = array(
        'type'            => 'yearly',
        'limit'           => '',
        'format'          => 'option', 
        'before'          => '',
        'after'           => '',
        'show_post_count' => false,
        'echo'            => 1,
        'order'           => 'DESC',
        'post_type'     => 'press'
    ); ?>

    <!-- CATEGORIES PRESS -->
    <div class="cr-nav-cloud">
        <div class="container">
            <ul class="list-inline justify-content-center">

                <li class="list-inline-item cat-item  <?php echo $activeCat; ?>">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>press" title="all">
                        All
                    </a>
                </li>

                <?php wp_list_categories(array(
                    'title_li' => '',
                    'walker'   => new Walker_Category_Class(),
                    'taxonomy' => 'press_category',
                )); ?> 

                <li class="list-inline-item  styled-select">
                    <select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
                        <option value="<?php echo esc_url( home_url( '/' ) ); ?>press"><?php echo esc_attr( __( 'All Years' ) ); ?></option> 
                        <?php add_filter("get_archives_link", "get_archives_link_mod"); ?>
                        <?php wp_get_archives($args); ?>
                    </select> 
                
                </li>

            </ul>

        </div>
    </div>
<?php endif; ?>