<?php 
    $title = $layout['title'] ?? '';
    $size = $layout['size'] ?? false;
    $video = $layout['video'] ?? '';
?>

<?php if ( !empty($video) ) : ?>
<div class="cr-section-content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="text-center row justify-content-center  <?php echo $size ? 'col-md-8' : 'col' ; ?> ">

                <div class="youtube__container">
                    <div class="youtube">
                        <?php echo $video; ?>
                    </div>
                </div>

                <?php if ( !empty($title) ) : ?>
                    <figcaption class="text-center"><?php echo $title; ?></figcaption>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>
<?php endif; ?>