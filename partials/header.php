<?php
    // GET CUSTOM FIELDS DATA
    if (function_exists('set_acf_conf')) { 
        $meta_data = set_acf_conf('header', false, $page_id); 
        $overview_data = set_acf_conf('overview', false, $page_id); 
    }


    $headerfixed = $meta_data['header_fixed'] ?? false;
    $call2actionScript_big = $meta_data['call_2_action_script_big'] ?? false;
    $overview_type = $overview_data['type'] ?? '';

    // STYLE
    $style = $meta_data['style'] ?? '';



    // TITLE
    $title = !empty($meta_data['title']) ? $meta_data['title'] : get_the_title($page_id);



    // DESCRIPTION
    $description = $meta_data['lead'] ?? '';



    // IMAGE
    $imageId = $meta_data['image'] ?? '';
    $image = $imageId ? 'style="background-image: url('. wp_get_attachment_image_url($imageId, 'header') .'); "' : '';

    $imageClass = $imageId ? 'cr-bg-image' : '';
    // if image is a graphic
    $imageSize = $meta_data['imagesize'] ?? '';
    if (!empty($imageSize) && !empty($image)) {
        $imageClass = 'cr-bg-graphic';
    }

    // PRESS OVERVIEW
    if ( get_post_type() == 'press' &&  is_archive()) {
        $image = 'style="background-image: url('. get_template_directory_uri() . '/img/gfx_blog_default.png' .'); "';
        $imageClass = '';
    }

    // IMAGE WRAPPER
    $wrapperClass = 'cr-section-header-wrapper  cr-section-header-wrapper--small';
    if (!empty( $style ) || !empty( $image ) ) {
        $wrapperClass = 'cr-section-header-wrapper';
    }

    // if startpage
    if ( is_front_page() ) {
        $wrapperClass = 'cr-section-header-wrapper cr-header-home';
    }



    // VIDEO
    $videos = $meta_data['videos'] ?? '';
    $videoOutput = '';
    if ( !empty($videos) ) {
        foreach ($videos as $key => $video) {
            $videoId = $video['video'] ?? '';
            $videoType = $video['videotype'] ?? '';

            $videoOutput .= '
                <source src="'.wp_get_attachment_url($videoId).'" type="video/'.$videoType.'">
            ';
        }
    }


    // BUTTON 1
    $url  = $meta_data['url']['url'] ?? '';
    $label  = $meta_data['url']['title'] ?? $url;
    if (empty($label)) {
        $label = $url;
    }
    $target  = $meta_data['url']['target'] ?? '';
    if (!empty($target)) {
        $target = 'target="'. $target .'" rel="noopener"';
    }

    // BUTTON 2
    $url2 = $meta_data['url2']['url'] ?? '';
    $target2  = $meta_data['url2']['target'] ?? '';
    if (!empty($target)) {
        $target2 = 'target="'. $target2 .'" rel="noopener"';
    }

    $label2  = $meta_data['url2']['title'] ?? '';
    if (empty($label2)) {
        $label2 = $url2;
    }



    // if archive
    $blogId = '';
    $pressId = '';
    if (is_archive()) {
        $title = post_type_archive_title('', false);
        $description = get_the_archive_description();

        if (get_post_type() == 'press' || get_post_type() == 'post') {

            if (!empty(single_cat_title('', false))) {
                $title = single_cat_title('', false);
            }

            if (get_post_type() == 'post') {
                $blogId = get_page_by_title( 'Blog', false, 'page' )->ID ?? '';
            }

            if (get_post_type() == 'press') {
                $pressId = get_page_by_title( 'Press', false, 'page' )->ID ?? '';
            }
        }
    }





    // no background color, image or call 2 action
    $headerClass = '';
    if ( empty($call2actionTop) && empty($style) && empty($image) ) {
        $headerClass = 'bottomless';
    }

    // if thankyou
    if ( is_page_template('page-thankyou.php') ) {
        $headerClass = '';
    }

    if ($call2actionScript_big) {
        $headerClass .= ' cr-header-cta';
    }

    if ($headerfixed) {
        $headerClass .= ' cr-header-large';
    }
    



    if (get_post_type() == 'post' && empty($style)) {
        $style = 'cyan';
    }

    // FALLBACK EMPTY BLOG IMAGE
    if (get_post_type() == 'post' && empty($image)) {
        $image = 'style="background-image: url('. get_template_directory_uri() . '/img/blog_article.png); background-size: cover; background-position: center;border-bottom:1px solid #e7e7e7;"';
        $style = '';
    }
?>



<?php
    // SHOW SUBNAVIGATION LIKE FILTERS
    if (function_exists('set_acf_conf')) {
        $subnav_data = set_acf_conf('subnav', false, $page_id);
    }

    $show_subnav = $subnav_data['show_subnavigation_items'] ?? false;
?>

<?php $template = get_post_meta( get_the_id(), '_wp_page_template', true ) ?? ''; ?>



<div class="<?php echo $wrapperClass; ?> ">
    <div class="cr-section-header <?php echo $style; ?> <?php echo $imageClass; ?>  <?php echo $headerClass; ?>" <?php echo $image; ?> >

        <div class="container">
            <div class="row">
                <div class="col">
                    <!-- BREADCRUMB -->
                    <?php if ( function_exists('nav_breadcrumb') ) nav_breadcrumb(); ?>

                    <?php if ( !is_page_template('page-download.php') ) : ?>
                        <!-- TITLE -->
                        <header>
                            <h1 class="cr-page-header"><?php echo $title; ?></h1>
                        </header>
                    <?php endif; ?>
                </div>
            </div>


            <?php switch ($template) {
                case 'page-download.php':
                    // DOWNLOAD HEADER
                    if (file_exists(get_template_directory() . '/partials/header/header_download.php') ) {
                        include(locate_template('partials/header/header_download.php'));
                    }
                    break;
                case 'page-getcrate.php':
                    if (file_exists(get_template_directory() . '/partials/header/header_getcrate.php') ) {
                        include(locate_template('partials/header/header_getcrate.php'));
                    }
                    break;
                default:
                    if (file_exists(get_template_directory() . '/partials/header/header_default.php') ) {
                        include(locate_template('partials/header/header_default.php'));
                    }
                    break;
            } ?>
        </div>

        <!-- Videos -->
        <?php if (!empty($videos)) : ?>
            <div class="cr-video-container">
              <video playsinline="" autoplay="" muted="" loop="" poster="assets/header-bg-1.png" id="bgvid">
                  <?php echo $videoOutput; ?>
              </video>
            </div>
        <?php endif; ?>
    </div>

    <?php 
        if (!($call2actionScript_big)) {
            // CALL 2 ACTION
            if (file_exists(get_template_directory() . '/partials/header/header_call2action.php') ) {
                include(locate_template('partials/header/header_call2action.php'));
            } 
        }
    ?>

    <?php 
        // FILTER ACTIVE CLASS
        $activeCat = '';
        if ( !is_archive() ) {
            $activeCat = 'current-cat active';
        }
    ?>

    <?php 
        // FILTER POST
        if (file_exists(get_template_directory() . '/partials/header/header_postfilter.php') ) {
            include(locate_template('partials/header/header_postfilter.php'));
        } 
    ?>

    <?php 
        // FILTER PRESS
        if (file_exists(get_template_directory() . '/partials/header/header_pressfilter.php') ) {
            include(locate_template('partials/header/header_pressfilter.php'));
        } 
    ?>

    <?php 
        // FILTER SUBNAV
        if ($show_subnav && file_exists(get_template_directory() . '/partials/header/header_subnav.php') ) {
            include(locate_template('partials/header/header_subnav.php'));
        } 
    ?>
</div>



<?php  if ( ($call2actionScript_big) ) : ?>
    <!-- CALL 2 ACTION SCRIPT BIG -->
    <?php
        
        if (file_exists(get_template_directory() . '/partials/header/header_call2action_big.php') ) {
            include(locate_template('partials/header/header_call2action_big.php'));
        } 
    ?>
<?php endif; ?>