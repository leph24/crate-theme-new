<?php 
    $title = $layout['title'] ?? false;
    $subtitle = $layout['subtitle'] ?? false;

    // BUTTON 1
    $link = $layout['link'] ?? false;
    $url =  $link['url'] ?? false;
    $target = $link['target'] ?? false;
    if (!empty($target)) {
        $target = 'target="'. $target .'" rel="noopener"';
    }
    $buttontext = $link['title'] ?? false;

    // BUTTON 2
    $link_2 = $layout['link_2'] ?? false;
    $url_2 =  $link_2['url'] ?? false;
    $target_2 = $link_2['target'] ?? false;
    if (!empty($target_2)) {
        $target_2 = 'target="'. $target_2 .'" rel="noopener"';
    }
    $buttontext_2 = $link_2['title'] ?? $url_2;

    $teammembers = $layout['team_members'] ?? false;

    $no_margin = $layout['no_margin'] ?? false;

    $class_section_content = '';
    if ( $no_margin ) {
        $class_section_content = 'no_margin';
    }
?>

<div class="cr-section-content  <?php echo $class_section_content; ?>">
    <div class="container">

        <?php if ($title || $subtitle) : ?>
            <div class="row justify-content-center">
              <div class="cr-single-header col-md-8">
                <div class="cr-box-tag">
                   <?php if ($subtitle) : ?>
                        <div><?php echo $subtitle; ?></div>
                    <div class="cr-seperator cr-seperator-thin cr-box-seperator"></div>
                  <?php endif; ?>
                </div>
                <?php if ($title) : ?>
                    <h2><?php echo $title; ?></h2>
                <?php endif; ?>
              </div>
            </div>
        <?php endif; ?>


        <div class="row justify-content-center">
            <div class="col">
                <div class="row">

                    <?php if ( is_array($teammembers) ) : ?>
                        <?php foreach ($teammembers as $key => $teammember) : ?>

                            <?php 
                                $imageId = $teammember['image'] ?? '';
                                $image = false;
                                if ($imageId) {
                                    $image = wp_get_attachment_image( $imageId, 'slidernav', false,['class'=>'img-fluid'] );
                                }

                                $subtitle = $teammember['subtitle'] ?? '';
                                $title = $teammember['title'] ?? '';

                                $imageId_2 = $teammember['image_2'] ?? false;
                                
                                $image_2 = false;
                                if ($imageId_2) {
                                $image_2 = wp_get_attachment_image( $imageId_2, 'slidernav',false, ['class'=>'img-fluid cr-swap-image-top'] );
                                }
                            ?>

                            <div class="cr-people-single col-6 col-sm-4 col-md-3 col-lg-2">
                                
                                <?php if ($image_2) : ?>
                                <div class="cr-swap-image">
                                  <?php echo $image_2; ?>
                                  <?php echo $image; ?>
                                </div>
                                <?php else: ?>
                                    <?php echo $image; ?>
                                <?php endif; ?>
                

                                <p><?php echo $title ?><br>
                                    <span class="text-muted"><?php echo $subtitle ?></span>
                                </p>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>

                </div>
            </div>
        </div>



        <?php if ($link || $link_2) : ?>
            <div class="row justify-content-center">
                <?php if ($link) : ?>
                    <a href="<?php echo $url; ?>" class="mx-2 btn btn-primary btn-lg"  <?php echo $target; ?> title="<?php echo $buttontext; ?>"><?php echo $buttontext; ?></a>
                <?php endif; ?>

                <?php if ($link_2) : ?>
                    <a href="<?php echo $url_2; ?>" class="mx-2 btn btn-secondary btn-lg"  <?php echo $target; ?> title="<?php echo $buttontext_2; ?>"><?php echo $buttontext_2; ?></a>
                <?php endif; ?>
            </div>
        <?php endif; ?>

    </div>
</div>

    
