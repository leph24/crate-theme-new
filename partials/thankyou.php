<?php $downloadtype = $_GET['download'] ?? ''; ?>

<?php 
    $arrContextOptions = array(
      "ssl" => array(
        "verify_peer" => false,
        "verify_peer_name" => false,
      )
    );
    $url = "https://crate.io/releases.json";
    $json = file_get_contents($url, false, stream_context_create($arrContextOptions));
    $json_data = json_decode($json, true);

    $stableVersion = $json_data['stable']['version'] ?? '';
    $stableDate = $json_data['stable']['date'] ?? '';
    $stable = $json_data['stable']['downloads']['tar.gz']['url'] ?? '';
    $stableAsc = $json_data['stable']['downloads']['tar.gz']['asc'] ?? '';

    $testingDate = $json_data['testing']['date'] ?? '';
    $testingVersion = $json_data['testing']['version'] ?? '';
    $testing = $json_data['testing']['downloads']['tar.gz']['url'] ?? '';
    $testingAsc = $json_data['testing']['downloads']['tar.gz']['asc'] ?? '';

    $nightly = $json_data['nightly']['downloads']['tar.gz']['url'] ?? '';
    $nightlyVersion = $json_data['nightly']['version'] ?? '';


    $downloadurl = '';
    $directdownload = '';
    switch ($downloadtype) {
      case 'tar':
        $downloadurl = $stable;
        $directdownload = 'js-download-now';
        break;
      case 'tarNightly':
        $downloadurl = $nightly;
        $directdownload = 'js-download-now';
        break;
      case 'tarTest':
        $downloadurl = $testing;
        $directdownload = 'js-download-now';
        break;
      case 'deb':
        $downloadurl = 'https://crate.io/docs/crate/guide/en/latest/deployment/linux/debian.html';
        break;
      case 'rpm':
        $downloadurl = 'https://crate.io/docs/crate/guide/en/latest/deployment/linux/red-hat.html';
        break;
      case 'docker':
        $downloadurl = 'https://crate.io/docs/crate/guide/en/latest/deployment/containers/docker.html';
        break;
      case 'ubuntu':
        $downloadurl = 'https://crate.io/docs/crate/guide/en/latest/deployment/linux/ubuntu.html';
        break;
      default:
        $downloadurl = $stable;
        break;
    }
?>

<div class="cr-section-content mt-0">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <?php if ($downloadtype == 'quickstart') : ?>
          <div>
            Just copy this command and run it in your CLI:
          </div>
          <div class="d-flex">
            <pre class="cr-pre-header mb-0 mr-2 language-bash" style="width:100%"><code>$ </code><code class=" mb-0 language-bash" id="code1">bash -c "$(curl -L try.crate.io)"</code></pre>
            <a href="#" class="btn btn-primary codebut" data-clipboard-target="#code1" title="Copy">Copy</a>
          </div>
          To get started with some demo data visit this <a target="_blank" href="/docs/crate/getting-started/en/latest/first-use/import.html" title="page">page</a>.
          <br><br>
          
        <?php else:  ?>
          <div class="lightcyan p-4 mb-5 text-center">
            <?php if (empty($directdownload)) : ?>
              Here is the link to your software:
            <?php endif; ?>

            <?php if (!empty($directdownload)) : ?>
               <br>Your download should begin automatically. If not please use the following link: 
             <?php endif; ?>

            <a id="<?php echo $directdownload; ?>" target="_blank" download" href="<?php echo $downloadurl; ?>" title="Download">Download</a>
          </div>
        <?php endif; ?>

        <h3>If you need help using CrateDB, try here:</h3>
        <ul class="list-unstyled">
          <li><a href="https://crate.io/docs/support/slackin/" target="_blank" class="cr-link-arrow">CrateDB Slack Support Channel
            <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <polygon id="Line-2" points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
            </svg>
          </a></li>
          <li><a href="https://stackoverflow.com/tags/crate" target="_blank" class="cr-link-arrow">CrateDB on StackOverflow
            <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <polygon id="Line-2" points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
            </svg>
          </a></li>
          <li><a href="https://twitter.com/crateio" target="_blank" class="cr-link-arrow">CrateDB Twitter
            <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <polygon id="Line-2" points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
            </svg>
          </a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<script>
  var link = document.getElementById("js-download-now");
  link.click()
</script>