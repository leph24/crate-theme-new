<?php 
    $title = $layout['title'] ?? '';
    $subtitle = $layout['tag'] ?? '';
    $description = $layout['description'] ?? '';

    $videos = $layout['repeater'] ?? ''; 
?>

<?php if ( (count($gallery) > 0) || ($title || $tag || $description) ) : ?>
    <div class="cr-section-content">
        <div class="container">

            <?php if (
                $subtitle || $title || $description
            ) : ?>
                <div class="row justify-content-center">
                    <div class="cr-single-header  col  col-md-8">
                        <?php if ($subtitle) : ?>
                            <div class="cr-box-tag">
                                <?php echo $subtitle; ?>
                                <div class="cr-seperator cr-seperator-thin cr-box-seperator"></div>
                            </div>
                        <?php endif; ?>

                        <?php if ($title) : ?>
                            <h2><?php echo $title; ?></h2>
                        <?php endif; ?>

                        <?php if ($description) : ?>
                            <?php echo wpautop($description); ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (count($videos) > 0) : ?>
                <div class="row justify-content-center">
                    <div class="col">
                        <div class="row justify-content-center cr-videoGallery">
                            <?php foreach ($videos as $key => $video) : ?>
                                <?php 
                                    $videoUrl = $video['link'] ?? false; 
                                    $thumbnail = $video['thumbnail_url'] ?? false; 
                                ?>

                                <?php if ($videoUrl) : ?>
                                    <div class="cr-img-single col-5 col-md-3 col-lg-2">
                                        <a href="https://www.youtube.com/embed/<?php echo $videoUrl; ?>">
                                          <img class="img-fluid" src="https://img.youtube.com/vi/<?php echo $videoUrl; ?>/0.jpg">
                                          <div class="cr-play">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/video-play.png">
                                          </div>
                                        </a>
                                    </div>
                                <?php endif; ?>

                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <!-- END: Slider -->
        </div>
    </div>
<?php endif; ?>