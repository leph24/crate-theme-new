<?php
/**
 * WordPress Bootstrap Pagination
 */

function wp_bootstrap_pagination( $args = array() ) {
    
    $defaults = array(
        'range'           => 3,
        'custom_query'    => FALSE,
        'previous_string' => __( 'Previous', 'text-domain' ),
        'next_string'     => __( 'Next', 'text-domain' ),
        'before_output'   => '<div class="row justify-content-center"><div class="col"><ul class="pagination justify-content-center">',
        'after_output'    => '</ul></div></div>'
    );
    
    $args = wp_parse_args( 
        $args, 
        apply_filters( 'wp_bootstrap_pagination_defaults', $defaults )
    );
    
    $args['range'] = (int) $args['range'] - 1;
    if ( !$args['custom_query'] )
        $args['custom_query'] = @$GLOBALS['wp_query'];
    $count = (int) $args['custom_query']->max_num_pages;
    $page  = intval( get_query_var( 'paged' ) );
    $ceil  = ceil( $args['range'] / 2 );
    
    if ( $count <= 1 )
        return FALSE;
    
    if ( !$page )
        $page = 1;
    
    if ( $count > $args['range'] ) {
        if ( $page <= $args['range'] ) {
            $min = 1;
            $max = $args['range'] + 1;
        } elseif ( $page >= ($count - $ceil) ) {
            $min = $count - $args['range'];
            $max = $count;
        } elseif ( $page >= $args['range'] && $page < ($count - $ceil) ) {
            $min = $page - $ceil;
            $max = $page + $ceil;
        }
    } else {
        $min = 1;
        $max = $count;
    }
    
    $echo = '';
    $previous = intval($page) - 1;
    $previous = esc_attr( get_pagenum_link($previous) );
    
    $firstpage = esc_attr( get_pagenum_link(1) );

    $firstpageClass = '';
    $previousClass = '';
    $nextClass = '';
    $lastpageClass = '';

    if ( $firstpage && (1 == $page) ) {
        $firstpageClass = 'disabled';
    }

    $echo .= '<li class="page-item  '.$firstpageClass.'"><a class="page-link" href="' . $firstpage . '"><span aria-hidden="true">
                        <svg width="14px" height="12px" viewBox="0 0 14 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <path fill="#000000" id="icon-pagination-first" d="M7.36535498,10.0039794 L5.94716759,11.414208 L0.585792036,6.02257464 L5.99206348,0.585792036 L7.41025087,1.99602058 L3.40629378,6.02257464 L7.36535498,10.0039794 Z M13.365355,10.0039794 L11.9471676,11.414208 L6.58579204,6.02257464 L11.9920635,0.585792036 L13.4102509,1.99602058 L9.40629378,6.02257464 L13.365355,10.0039794 Z"></path>
                        </svg>
                    </span><span class="sr-only">First</span></a></li>';

    if ( $previous && (1 == $page) ) {
        $previousClass = 'disabled';
    }
    
    $echo .= '<li class="page-item  '.$previousClass.'"><a class="page-link" href="' . $previous . '" title="' . __( 'previous', 'text-domain') . '"><span aria-hidden="true">
                        <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <polygon id="icon-pagination-previous" transform="translate(3.998021, 6.000000) scale(-1, 1) translate(-3.998021, -6.000000) " points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
                        </svg>
                    </span>
                    <span class="sr-only">Previous</span></a></li>';

    if ( !empty($min) && !empty($max) ) {
        for( $i = $min; $i <= $max; $i++ ) {
            if ($page == $i) {
                $echo .= '<li class="page-item active"><span class="page-link">' . str_pad( (int)$i, 1, '0', STR_PAD_LEFT ) . '</span></li>';
            } else {
                $echo .= sprintf( '<li class="page-item"><a class="page-link" href="%s">%2d</a></li>', esc_attr( get_pagenum_link($i) ), $i );
            }
        }
    }
    
    $next = intval($page) + 1;
    $next = esc_attr( get_pagenum_link($next) );
    if ($next && ($count == $page) ) {
        $nextClass = 'disabled';
    }

    $echo .= '<li class="page-item '.$nextClass.'"><a class="page-link" href="' . $next . '" title="' . __( 'next', 'text-domain') . '"><span aria-hidden="true">
                        <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <polygon id="icon-pagination-next" points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
                        </svg>
                    </span>
                    <span class="sr-only">Next</span></a></li>';
    
    $lastpage = esc_attr( get_pagenum_link($count) );
    if ( $lastpage && ($count == $page)) {
        $lastpageClass = 'disabled';
    }

    $echo .= '<li class="page-item '.$lastpageClass.'"><a class="page-link" href="' . $lastpage . '"><span aria-hidden="true">
                        <svg width="14px" height="12px" viewBox="0 0 14 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <path id="icon-pagination-last" d="M0.630687932,10.0039794 L4.58974913,6.02257464 L0.585792036,1.99602058 L2.00397942,0.585792036 L7.41025087,6.02257464 L2.04887532,11.414208 L0.630687932,10.0039794 Z M6.63068793,10.0039794 L10.5897491,6.02257464 L6.58579204,1.99602058 L8.00397942,0.585792036 L13.4102509,6.02257464 L8.04887532,11.414208 L6.63068793,10.0039794 Z"></path>
                        </svg>
                    </span>
                    <span class="sr-only">Last</span></a></li>';

    if ( isset($echo) ) {
        echo $args['before_output'] . $echo . $args['after_output'];
    }
}
