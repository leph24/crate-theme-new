<?php
/**
 * Template Name: License
 */

// PAGE REDIRECT
require_once(locate_template('partials/external_url.php'));

// FROTNEND EDITOR
if ( is_user_logged_in() && $GLOBALS[ 'frontendeditor' ] && function_exists('acf_form_head') ) {
    acf_form_head();
}

get_header(); 
?>

<?php
while ( have_posts() ) : the_post(); ?>
    <?php if ( is_user_logged_in() && $GLOBALS[ 'frontendeditor' ] && function_exists('acf_form') ) : ?>
        <aside class="admin-sidebar">
            <?php echo acf_form(); ?>
        </aside>
    <?php endif; ?>

    <?php get_template_part( 'template-parts/content', 'page' ); ?>

    <?php 
    // If comments are open or we have at least one comment, load up the comment template.
    if ( comments_open() || get_comments_number() ) :
        comments_template();
    endif; ?>

<?php endwhile; ?>

<?php
get_sidebar();
get_footer();
