<header id="Top" class="cr-section-top border border-light border-left-0 border-right-0 border-top-0">
    <div class="container">
        <nav id="nav-main" class="navbar navbar-expand-lg navbar-light">
            <?php the_custom_logo(); ?>

            <button class="navbar-toggler cr-navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span class="cr-navtoggle cr-navtoggle-top"></span>
              <span class="cr-navtoggle cr-navtoggle-mid"></span>
              <span class="cr-navtoggle cr-navtoggle-bottom"></span>
            </button>

            <?php
                wp_nav_menu( array(
                    'menu'              => 'primary',
                    'theme_location'    => 'menu-1',
                    'depth'             => 2,
                    'container_class'   => 'collapse navbar-collapse',
                    'container_id'      => 'navbarNavDropdown',
                    'menu_class'        => 'navbar-nav',
                    'walker'            => new WP_Bootstrap_Navwalker()
                ) );
            ?>

        </nav>
    </div>
</header>