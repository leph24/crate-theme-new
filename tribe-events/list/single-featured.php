<?php
/**
 * List View Single Featured Event
 * This file contains one featured event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-featured.php
 *
 * @version 4.6.3
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Setup an array of venue details for use later in the template
$venue_details = tribe_get_venue_details();

$venue_address = tribe_get_address();

// Venue
$has_venue_address = ( ! empty( $venue_details['address'] ) ) ? ' location' : '';

// Organizer
$organizer = tribe_get_organizer();

$image = '<img src="'.get_template_directory_uri().'/img/gfx_blog_default.png" alt="Event Image Placeholder">';
if ( !empty(tribe_event_featured_image( null, 'large' )) ) {
	$image = tribe_event_featured_image( null, 'large' );
}

$colClass = '';
if (empty($image)) {
	$colClass = 'col-md-8';
}
?>




<div class="cr-feature cr-feature--left">
    <div class="row cr-feature-body justify-content-center">
    	<?php if (!empty($image)) : ?>
	        <div class="col-md-4">
	            <a href="<?php echo esc_url( tribe_get_event_link() ); ?>" class="">
	                <!-- Event Image -->
					<?php echo $image ?>
	            </a>
	        </div>
	    <?php endif; ?>

        <div class="col <?php echo $colClass; ?>">
            <p class="cr-feature-tag">
                <!-- Event Meta -->
				<?php do_action( 'tribe_events_before_the_meta' ) ?>

					<?php if ( $venue_details ) : ?>
						<!-- Venue Display Info -->
						<?php

							// These details are already escaped in various ways earlier in the process.
                            if (!empty($venue_details['linked_name'])) {
                                echo $venue_details['linked_name'];
                            }
						?>
					<?php endif; ?>
				<?php do_action( 'tribe_events_after_the_meta' ) ?>
            </p>

            <?php do_action( 'tribe_events_before_the_event_title' ) ?>
            <h2>
                <a href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
                    <?php the_title() ?>
                </a>
            </h2>
            <?php do_action( 'tribe_events_after_the_event_title' ) ?>

            <p class="cr-feature-mute">
        		<?php do_action( 'tribe_events_before_the_meta' ) ?>
            	    <?php echo tribe_events_event_schedule_details() ?>
    	    	<?php do_action( 'tribe_events_after_the_meta' ) ?>
            </p>

			<!-- Event Content -->
			<?php do_action( 'tribe_events_before_the_content' ); ?>
			<?php echo tribe_events_get_the_excerpt( null, wp_kses_allowed_html( 'post' ) ); ?>

            <a href="<?php echo esc_url( tribe_get_event_link() ); ?>" rel="bookmark" class="cr-link-arrow">Read more
                <svg width="8px" height="12px" viewBox="0 0 8 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <polygon points="0.630687932 10.0039794 2.04887532 11.414208 7.41025087 6.02257464 2.00397942 0.585792036 0.585792036 1.99602058 4.58974913 6.02257464"></polygon>
                </svg>
            </a>

            <?php do_action( 'tribe_events_after_the_content' ); ?>
        </div>
    </div>

    <div class="row cr-feature-footer justify-content-center">
    	<?php if (!empty($image)) : ?>
	        <div class="col-md-4"></div>
	    <?php endif; ?>

        <div class="col <?php echo $colClass; ?>">
            <div class="cr-seperator cr-seperator-thin cr-seperator--left"></div>
        </div>
    </div>
</div>
