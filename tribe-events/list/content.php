<?php
/**
 * List View Content Template
 * The content template for the list view. This template is also used for
 * the response that is returned on list view ajax requests.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/content.php
 *
 * @package TribeEventsCalendar
 * @version  4.3
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
} ?>

<div id="tribe-events-content" class="tribe-events-list">

    <?php
    /**
     * Fires before any content is printed inside the list view.
     */
    do_action( 'tribe_events_list_before_the_content' );
    ?>

	<!-- List Title -->
	<?php do_action( 'tribe_events_before_the_title' ); ?>

	<div class="cr-section-header-wrapper">
	    <div class="cr-section-header  cr-bg-graphic  " style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/header-bg-1.png);">
	        <div class="container">
	            <div class="row">
	                <div class="col">

	                    <!-- BREADCRUMB -->
	                    <?php if (function_exists('nav_breadcrumb')) nav_breadcrumb(); ?>
	                    <!-- TITLE -->
	                    <header>
	                        <h1 class="cr-page-header"><?php echo tribe_get_events_title() ?></h1>
	                    </header>
	                </div>
	            </div>

				<div class="row justify-content-center">
				    <div class="col-12 col-lg-8">
				    	<div class="lead">
				    		<p>
				    			We host regular webinars and meetups, and list recommended events on the topic of database technology. Check back frequently for updates.
				    		</p>
				    	</div>
				    </div>
				</div>
	        </div>
	    </div>
	</div>
	<?php do_action( 'tribe_events_after_the_title' ); ?>



    <!-- List Footer -->
    <?php do_action( 'tribe_events_before_footer' ); ?>
    <div id="tribe-events-footer">

        <!-- Footer Navigation -->
        <?php do_action( 'tribe_events_before_footer_nav' ); ?>
        <?php tribe_get_template_part( 'list/nav', 'footer' ); ?>
        <?php do_action( 'tribe_events_after_footer_nav' ); ?>

    </div>
    <!-- #tribe-events-footer -->
    <?php do_action( 'tribe_events_after_footer' ) ?>




	<div class="cr-section-content">
	    <div class="container">
	        <div class="row justify-content-center">
	            <div class="col">
					<!-- Notices -->
					<?php tribe_the_notices() ?>

				    <!-- Events Loop -->
				    <?php if ( have_posts() ) : ?>
				        <?php do_action( 'tribe_events_before_loop' ); ?>
				        <?php tribe_get_template_part( 'list/loop' ) ?>
				        <?php do_action( 'tribe_events_after_loop' ); ?>
				    <?php endif; ?>
				</div>
			</div>
		</div>
	</div><!-- .tribe-events-loop -->



</div><!-- #tribe-events-content -->
