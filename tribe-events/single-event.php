<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.3
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

$event_id = get_the_ID();

?>


<article class="">

	<!-- HEADER -->
	<div class="cr-section-header-wrapper ">
	    <div class="cr-section-header  bottomless">
	        <div class="container">
	            <div class="row">
	                <div class="col">
	                	<!-- BREADCRUMB -->
	                    <?php if (function_exists('nav_breadcrumb')) nav_breadcrumb(); ?>

	                	<header>
                            <?php the_title( '<h1 class="cr-page-header">', '</h1>' ); ?>
                        </header>

	                </div>
                </div>

                <!-- DATE -->
                <div class="row justify-content-center">
    				<div class="col-12 col-lg-8">
                    	<div class="cr-tag-article">
                    		<?php echo tribe_events_event_schedule_details( $event_id, '<span>', '</span>' ); ?>
							<?php if ( tribe_get_cost() ) : ?>
								<span class=""><?php echo tribe_get_cost( null, true ) ?></span>
							<?php endif; ?>
                    	</div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- CONTENT -->
    <div class="cr-section-content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
            		<!-- Notices -->
					<?php tribe_the_notices() ?>

					<?php while ( have_posts() ) :  the_post(); ?>
						<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<!-- Event featured image, but exclude link -->
							<?php echo tribe_event_featured_image( $event_id, 'full', false ); ?>

							<!-- Event content -->
							<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
							<div class="tribe-events-single-event-description tribe-events-content">
								<?php the_content(); ?>
							</div>
							<!-- .tribe-events-single-event-description -->
							<?php do_action( 'tribe_events_single_event_after_the_content' ) ?>

							<!-- Event meta -->
							<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
							<?php tribe_get_template_part( 'modules/meta' ); ?>
							<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
						</div> <!-- #post-x -->
						<?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
					<?php endwhile; ?>

                </div>
            </div>
        </div>
    </div>
</article><!-- #tribe-events-content -->
