<?php
/**
* The header for our theme
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package crate
*/
$GLOBALS[ 'frontendeditor' ] = false;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>



    <?php if ( is_user_logged_in() && $GLOBALS[ 'frontendeditor' ] ) : ?>
        <!-- Hamburger -->
        <input class="menubutton__input" type="checkbox" id="burger">
        <label class="menubutton" for="burger">
            <span class="dashicons dashicons-menu"></span>
        </label>

        <div class="admincontainer">
    <?php endif; ?>

    <?php include get_template_directory() . '/inc/header.php'; ?>

    <main class="main">