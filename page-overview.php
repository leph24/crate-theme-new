<?php
/**
* Template Name: Overview
*
*/

// PAGE REDIRECT
require_once(locate_template('partials/external_url.php'));

// FROTNEND EDITOR
if ( is_user_logged_in() && $GLOBALS[ 'frontendeditor' ] && function_exists('acf_form_head') ) {
    acf_form_head();
}

get_header();

?>

<?php if ( is_user_logged_in() && $GLOBALS[ 'frontendeditor' ] && function_exists('acf_form') ) : ?>
    <aside class="admin-sidebar">
        <?php echo acf_form(); ?>
    </aside>
<?php endif; ?>

<?php 
    if (function_exists('set_acf_conf')) {
        $overview_data = set_acf_conf('overview');
    }

    $overview_type = $overview_data['type'] ?? '';

    $specialClass = '';
    if ( ($overview_type == 'white_papers') || ($overview_type == 'webinars')  || ($overview_type == 'videos') ) {
        $specialClass = 'row';
    }
?>

<?php require_once(locate_template('partials/header.php')); ?>

<div class="cr-section-content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col <?php echo $specialClass; ?>">
                <?php if ( have_posts() ) : ?>
                    <?php $args = array( 
                        'post_type' => $overview_type,
                        'posts_per_page' => 10,
                        'paged' => get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
                    );

                    $loop = new WP_Query( $args );

                    $temp_query = $wp_query;
                    $wp_query   = NULL;
                    $wp_query   = $loop; 
                    $count = 0;
                    ?>



                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                        <?php 
                            switch ($count % 6) {
                                case 0:
                                    $style = "cyan";
                                    break;
                                case 1:
                                    $style = "lightcyan";
                                    break;
                                case 2:
                                    $style = "grey";
                                    break;
                                case 3:
                                    $style = "cyan";
                                    break;
                                case 4:
                                    $style = "lightcyan";
                                    break;
                                case 5:
                                    $style = "grey";
                                    break;
                                default:
                                    # code...
                                    break;
                            }
                        ?>

                        <?php include(locate_template('partials/overview_item.php')); ?>

                        <?php $count++; ?>
                    <?php endwhile; ?>

                    <?php if ( function_exists('wp_bootstrap_pagination') ) wp_bootstrap_pagination(); ?>

                    <?php wp_reset_query(); ?>
                <?php else : ?>
                    <?php get_template_part( 'template-parts/content-post', 'none' ); ?>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>


<?php
    // SECTIONS
    if (function_exists('set_acf_conf')) {
        $meta_data = set_acf_conf('sections');
    }
?>

<?php if ( !empty($meta_data) ) : ?>
    <?php foreach ($meta_data as $key => $meta) : ?>
        <?php if ( is_array($meta) ) : ?>
            <?php foreach ($meta as $layout) : ?>
                <?php if (file_exists(get_template_directory() . '/partials/' . $layout['acf_fc_layout'] . '.php')) : ?>
                    <?php include(locate_template('partials/' . $layout['acf_fc_layout'] . '.php')); ?>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>


<?php
get_sidebar();
get_footer();
