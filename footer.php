<?php
/**
* The template for displaying the footer
*
* Contains the closing of the #content div and all content after.
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package crate
*/
?>

</main>

<div class="cr-section-share py-5">
    <div class="container">

        <div class="row justify-content-center">
            <?php if ( (get_post_type() == 'post') && !('page' == get_option( 'show_on_front' )) ) : ?>
                <div class="col-12 text-center">

                </div>
            <?php endif; ?>

            <div class="col-12 text-center">
                <a class="cr-top" href="#Top">Go to Top</a>
            </div>
        </div>

    </div>
</div>

<footer class="footer cr-section-footer border border-light border-left-0 border-right-0 border-bottom-0">
    <div class="container">
        <div class="row">
            <?php if ( has_nav_menu( 'footer-1' ) ) : ?>
                <div class="cr-wrapper-footer-linkblock col-sm-12 col-md-2">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'footer-1',
                        'menu_id'        => '',
                        'items_wrap' => '%3$s',
                        'walker' => new Le_Walker_Footer(),
                    ) );
                    ?>
                </div>
            <?php endif; ?>


            <?php if ( has_nav_menu( 'footer-6' ) ) : ?>
                <div class="cr-wrapper-footer-linkblock col-sm-12 col-md-2">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'footer-6',
                        'menu_id'        => '',
                        'items_wrap' => '%3$s',
                        'walker' => new Le_Walker_Footer(),
                    ) );
                    ?>
                </div>
            <?php endif; ?>

            <?php if ( has_nav_menu( 'footer-2' ) ) : ?>
                <div class="cr-wrapper-footer-linkblock col-sm-12 col-md-2">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'footer-2',
                        'menu_id'        => '',
                        'items_wrap' => '%3$s',
                        'walker' => new Le_Walker_Footer(),
                    ) );
                    ?>
                </div>
            <?php endif; ?>

            <?php if ( has_nav_menu( 'footer-3' ) ) : ?>
                <div class="cr-wrapper-footer-linkblock col-sm-12 col-md-2">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'footer-3',
                        'menu_id'        => '',
                        'items_wrap' => '%3$s',
                        'walker' => new Le_Walker_Footer(),
                    ) );
                    ?>
                </div>
            <?php endif; ?>

            <?php if ( has_nav_menu( 'footer-4' ) ) : ?>
                <div class="cr-wrapper-footer-linkblock col-sm-12 col-md-2">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'footer-4',
                        'menu_id'        => '',
                        'items_wrap' => '%3$s',
                        'walker' => new Le_Walker_Footer(),
                    ) );
                    ?>
                </div>
            <?php endif; ?>




            <div class="cr-wrapper-footer-linkblock col-sm-12 col-md-2">
                <a class="h6 collapsed disabled" data-toggle="collapse" aria-expanded="true" aria-controls="footer-linkblock6" href="#cr-footer-linkblock6">Follow us</a>
                <div id="cr-footer-linkblock6" class="collapse show">
                    <ul class="cr-follow-list list-inline">
                        <li class="list-inline-item">
                            <a href="https://twitter.com/crateio" target="_blank" rel="noopen">
                                <svg width="20px" height="20px" viewBox="0 0 23 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <path d="M22.443,2.334 C21.63,2.697 20.943,2.709 20.215,2.35 C21.154,1.788 21.197,1.393 21.536,0.331 C20.658,0.852 19.685,1.231 18.65,1.435 C17.822,0.553 16.642,0 15.335,0 C12.825,0 10.791,2.036 10.791,4.544 C10.791,4.9 10.831,5.247 10.908,5.58 C7.131,5.391 3.783,3.582 1.542,0.832 C1.151,1.503 0.926,2.284 0.926,3.117 C0.926,4.694 1.73,6.084 2.948,6.899 C2.203,6.875 1.503,6.671 0.891,6.331 C0.89,6.35 0.89,6.368 0.89,6.388 C0.89,8.59 2.456,10.426 4.536,10.844 C3.87,11.025 3.168,11.053 2.483,10.923 C3.062,12.727 4.74,14.041 6.728,14.078 C4.783,15.602 2.372,16.236 0,15.959 C2.012,17.248 4.399,18 6.965,18 C15.324,18 19.894,11.076 19.894,5.071 C19.894,4.873 19.89,4.678 19.881,4.483 C20.769,3.843 21.835,3.246 22.443,2.334" id="icon-follow-twitter" fill="#55D4F5"></path>
                                </svg>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.linkedin.com/company/crateio/" target="_blank" rel="noopen">
                                <svg width="20px" height="20px" viewBox="0 0 22 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <path d="M14.027,10.893 C12.922,10.893 12.027,11.789 12.027,12.893 L12.027,19.893 L7.027,19.893 C7.027,19.893 7.086,7.893 7.027,6.893 L12.027,6.893 L12.027,8.378 C12.027,8.378 13.575,6.935 15.964,6.935 C18.927,6.935 21.027,9.079 21.027,13.239 L21.027,19.893 L16.027,19.893 L16.027,12.893 C16.027,11.789 15.131,10.893 14.027,10.893 L14.027,10.893 Z M2.518,4.893 L2.487,4.893 C0.978,4.893 0,3.711 0,2.412 C0,1.083 1.007,0 2.547,0 C4.088,0 5.035,1.118 5.066,2.447 C5.065,3.747 4.088,4.893 2.518,4.893 L2.518,4.893 Z M5.027,19.893 L0.027,19.893 L0.027,6.893 L5.027,6.893 L5.027,19.893 Z" id="icon-follow-linkedin" fill="#55D4F5"></path>
                                </svg>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.youtube.com/user/CrateIO" target="_blank" rel="noopen">
                                <svg width="20px" height="20px" viewBox="0 0 24 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <path d="M16.538,8.531 L9.77,12.861 C9.688,12.914 9.594,12.941 9.5,12.941 C9.417,12.941 9.335,12.92 9.26,12.878 C9.1,12.792 9,12.623 9,12.441 L9,3.779 C9,3.596 9.1,3.428 9.26,3.34 C9.421,3.253 9.616,3.26 9.77,3.358 L16.539,7.69 C16.684,7.781 16.769,7.94 16.769,8.111 C16.769,8.281 16.682,8.439 16.538,8.531 M20.062,0 L3.938,0 C1.767,0 0,1.768 0,3.939 L0,13.06 C0,15.232 1.767,17 3.938,17 L20.062,17 C22.232,17 24,15.232 24,13.06 L24,3.939 C24,1.768 22.232,0 20.062,0" id="icon-follow-youtube" fill="#000000"></path>
                                </svg>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://github.com/crate/crate" target="_blank" rel="noopen">
                                <svg width="20px" height="20px" viewBox="0 0 22 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <path d="M20.4885914,7.214 C21.5085914,8.74 22.0635914,10.648 21.8845914,12.96 C21.6455914,16.086 19.0385914,19.5 15.9025914,19.5 L6.01459136,19.5 C2.88059136,19.5 0.273591357,16.086 0.0325913575,12.96 C-0.144408643,10.65 0.410591357,8.743 1.42859136,7.217 C1.31359136,7.012 -1.13140864,2.603 2.04259136,-2.02504463e-13 C4.03759136,-2.02504463e-13 7.08359136,3.285 7.09859136,3.302 C9.59459136,2.581 12.3895914,2.586 14.8795914,3.318 L14.8575914,3.303 C14.8575914,3.303 17.9165914,-0.001 19.9165914,-0.001 C23.1665914,2.665 20.5205914,7.236 20.5205914,7.236 L20.4885914,7.214 Z M14.9595914,17.5 C16.8935914,17.5 18.4595914,15.934 18.4595914,14 C18.4595914,8.031 14.3335914,10.469 10.9595914,10.469 C7.58459136,10.469 3.45959136,8.093 3.45959136,14 C3.45959136,15.934 5.02659136,17.5 6.95959136,17.5 L14.9595914,17.5 Z M14.5,12.5 C15.053,12.5 15.5,13.172 15.5,14 C15.5,14.828 15.053,15.5 14.5,15.5 C13.948,15.5 13.5,14.828 13.5,14 C13.5,13.172 13.948,12.5 14.5,12.5 Z M7.5,12.5 C8.052,12.5 8.5,13.172 8.5,14 C8.5,14.828 8.052,15.5 7.5,15.5 C6.948,15.5 6.5,14.828 6.5,14 C6.5,13.172 6.948,12.5 7.5,12.5 Z" id="icon-follow-github" fill="#000000"></path>
                                </svg>
                            </a>
                        </li>
                    </ul>
                    <div class="cr-nav-github">
                        <span></span>

                        <div class="cr-nav-github">
                            <a class="nav-link mt-1 github-button" href="https://github.com/crate/crate" data-icon="octicon-star" data-show-count="true" aria-label="Star crate/crate on GitHub">Star</a>
                        </div>
                    </div>
                    <?php if ( has_nav_menu( 'footer-5' ) ) : ?>
                        <div class="nav flex-column">
                            <?php
                            wp_nav_menu( array(
                                'theme_location' => 'footer-5',
                                'menu_id'        => '',
                                'menu_class' => 'collapse show',
                                'container' => 'div',
                                'items_wrap' => '%3$s',
                                'walker' => new Le_Walker_Footer_2(),
                            ) );
                            ?>
                            <a href="/erdf/"><img class="cr-logo-erdf" src="<?php echo get_template_directory_uri(); ?>/img/erdf.png?v1" alt="erdf"></a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>


        </div>
    </div>



    <div class="container">
        <div class="text-center">
            <div class="text-muted cr-people-single">
                <p>
                    &copy; <?php echo date('Y'); ?> Crate.io. All rights reserved.
                </p>
            </div>
        </div>
    </div>


</footer>

<?php if ( is_user_logged_in() && $GLOBALS[ 'frontendeditor' ] ) : ?>
</div>
<?php endif; ?>

<?php wp_footer(); ?>


<?php if (is_page_template('page-job.php')) : ?>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/recruiting.js?ver=20190116"></script>
<?php endif; ?>

<?php if (is_page_template('page-getcrate.php')) : ?>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/getcrate.js"></script>
<?php endif; ?>


<?php 
$showintercom = false;

$analyticskey = 'wuLdPltN095xODlvqBLGyr5JpvUbnW6r';
// todo if staging
if (false) {
    $analyticskey = 'qio2kmx6lv';
}

if (function_exists('set_acf_conf')) {
    $intercom = set_acf_conf('intercom');
}
?>

<?php foreach ($intercom as $key => $intercomdata) : ?>
    <?php $showintercom = $intercomdata; ?>
<?php endforeach; ?>

<?php $intercomvalue = 'true'; ?>
<?php if ($showintercom) : ?>
    <?php $intercomvalue = 'false'; ?>
<?php endif; ?>


<!-- SEGMENT -->
<script>
    !function(){
        var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on","setAnonymousId"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t){var e=document.createElement("script");e.type="text/javascript";e.async=!0;e.src=("https:"===document.location.protocol?"https://":"http://")+"cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n)};analytics.SNIPPET_VERSION="4.0.0";
        analytics.load('<?php echo $analyticskey; ?>');
        analytics.setAnonymousId(Cookies('uid'));

        analytics.page({},{Intercom:{hideDefaultLauncher: <?php echo $intercomvalue; ?> }});

    }}();
</script>



<?php
// SECTIONS
if (function_exists('set_acf_conf')) {
    $scripts_data = set_acf_conf('scripts');
}

$footer_script = $scripts_data['custom_javascript'] ?? '';
?>

<?php if ($footer_script) : ?>
    <script>
        <?php echo $footer_script; ?>
    </script>
<?php endif; ?>

<!-- MAKETO FALLBACK -->
<script>
    if (typeof MktoForms2 === 'undefined') {
        $('.marketofallback').show();
    }
</script>


<script async defer src="https://buttons.github.io/buttons.js"></script>
</body>
</html>