<?php
/**
 * crate functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package crate
 */

/**
 * Add custom surrogate keys
 */
add_action('purgely_pre_send_keys', 'custom_surrogate_keys');
function custom_surrogate_keys($keys_object) {
    $keys_object->add_key('wordpress');
}


if ( ! function_exists( 'crate_setup' ) ) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */

    $frontendeditor = false;
    function crate_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on crate, use a find and replace
         * to change 'crate' to the name of your theme in all the template files.
         */
        load_theme_textdomain( 'crate', get_template_directory() . '/languages' );

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support( 'post-thumbnails' );

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus( array(
            'menu-1' => esc_html__( 'Primary', 'crate' ),
            'footer-1' => esc_html__( 'footer-1', 'crate' ),
            'footer-2' => esc_html__( 'footer-2', 'crate' ),
            'footer-3' => esc_html__( 'footer-3', 'crate' ),
            'footer-4' => esc_html__( 'footer-4', 'crate' ),
            'footer-5' => esc_html__( 'footer-5', 'crate' ),
            'footer-6' => esc_html__( 'footer-6', 'crate' ),
        ) );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );

        // Set up the WordPress core custom background feature.
        add_theme_support( 'custom-background', apply_filters( 'crate_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        ) ) );

        // Add theme support for selective refresh for widgets.
        add_theme_support( 'customize-selective-refresh-widgets' );

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support( 'custom-logo', array(
            'height'      => 250,
            'width'       => 250,
            'flex-width'  => true,
            'flex-height' => true,
        ) );
    }
endif;
add_action( 'after_setup_theme', 'crate_setup' );


// CUSTOM LOGO
add_filter( 'get_custom_logo', 'change_logo_class' );
function change_logo_class( $html ) {

    $html = str_replace( 'custom-logo', 'cr-logo', $html );
    $html = str_replace( 'cr-logo-link', 'navbar-brand', $html );

    return $html;
}

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function crate_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'crate_content_width', 640 );
}
add_action( 'after_setup_theme', 'crate_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function crate_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'crate' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'crate' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'crate_widgets_init' );



add_action( 'wp_enqueue_scripts', 'register_jquery' );
    function register_jquery() {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', ( 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js' ), false, null, true );
    wp_enqueue_script( 'jquery' );
}



/**
 * Enqueue scripts and styles.
 */
function crate_scripts() {
    // wp_enqueue_style( 'crate-bootstrap', 'https://cdn.crate.io/libs/bootstrap/4.0.0/css/bootstrap.min.css' );
    // wp_enqueue_style( 'crate-slick', 'https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.css' );
    // wp_enqueue_style( 'crate-slick-theme', 'https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.min.css' );

    // wp_enqueue_style( 'crate-style', get_stylesheet_uri() );
    // wp_enqueue_style( 'crate-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
    // wp_enqueue_style( 'crate-prism', get_template_directory_uri() . '/css/prism.css' );
    // wp_enqueue_style( 'crate-theme', get_template_directory_uri() . '/css/styles.css' );

    // minified
    wp_enqueue_style( 'crate-style-min', get_template_directory_uri() . '/css/style.min.css', '', '20190214' );

    wp_enqueue_style( 'lightgallery', 'https://cdn.jsdelivr.net/npm/lightgallery@1.6.11/dist/css/lightgallery.min.css', '', '20190214' );
    

    // JS
    // wp_enqueue_script( 'crate-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20190214', true );

    // EXTERNAL CDN


    // wp_enqueue_script( 'crate-popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js', array('jquery'), null, true );
    // wp_enqueue_script( 'crate-bootstrap-js', 'https://cdn.crate.io/libs/bootstrap/4.0.0/js/bootstrap.min.js', array('jquery'), null, true );
    // wp_enqueue_script( 'crate-slick', 'https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js', array('jquery'), null, true );
    // wp_enqueue_script( 'crate-cookie', 'https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js', array('jquery'), true );
 
    // CLIPBOARD
    // wp_enqueue_script( 'crate-clipboard', 'https://cdn.crate.io/libs/clipboard/clipboard.min.js', array('jquery'), null, true );

    // PRISM
    // wp_enqueue_script( 'crate-prism', 'https://cdn.crate.io/libs/prism/prism.js', array('jquery'), null, true );

    // SEGMENT
    // wp_enqueue_script( 'crate-segment', 'https://cdn.crate.io/libs/segment/segment.js', array('jquery'), '20190214', true );


    // CDN - LIGHT GALLERY
    wp_enqueue_script( 'lightgallery', 'https://cdn.jsdelivr.net/npm/lightgallery@1.6.11/dist/js/lightgallery.min.js', array('jquery'), '20190214', true );

    wp_enqueue_script( 'lg-thumbnail', 'https://cdn.jsdelivr.net/npm/lightgallery@1.6.11/modules/lg-thumbnail.min.js', array('jquery'), '20190214', true );

    wp_enqueue_script( 'lg-video', 'https://cdn.jsdelivr.net/npm/lg-video@1.2.2/dist/lg-video.min.js', array('jquery'), '20190214', true );

    // MINIFIED BUNDLED
    wp_enqueue_script( 'crate-scripts', get_template_directory_uri() . '/js/min/script.min.js', array('jquery'), '20190214', true );


    

    // CUSTOM SCRIPT
    wp_enqueue_script( 'crate-script', get_template_directory_uri() . '/js/min/script.js', array('jquery'), '20190214', true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'crate_scripts' );


function my_deregister_scripts(){
  wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );




/**
 * LE WALKER
 */
class Le_Walker_Nav_Menu extends Walker_Nav_Menu {
    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        // var_dump($item);

        $classes = array();
        if( !empty( $item->classes ) ) {
            $classes = (array) $item->classes;
        }

        $active_class = '';
        if( in_array('current-menu-item', $classes) ) {
            $active_class = 'active';
        } else if( in_array('current-menu-parent', $classes) ) {
            $active_class = 'active-parent';
        } else if( in_array('current-menu-ancestor', $classes) ) {
            $active_class = 'active-ancestor';
        }

        $parent_class = '';
        if( in_array('menu-item-has-children', $classes) ) {
            $parent_class = 'dropdown';
        }

        $url = '';
        if( !empty( $item->url ) ) {
            $url = $item->url;
        }

        $output .= '
        <li class="nav-item  '. $active_class . ' ' . $parent_class . '">
            <a class="nav-link" href="' . $url . '">
                ' . $item->title . '
            </a>';
    }

    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
        $output .= '</li>';
    }
}

/**
 * LE WALKER
 */
class Le_Walker_Footer extends Walker_Nav_Menu {
    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        // var_dump($item->ID);

        $classes = array();
        if( !empty( $item->classes ) ) {
            $classes = (array) $item->classes;
        }

        $parent_class = '';
        if( in_array('menu-item-has-children', $classes) ) {
            $parent_class = 'dropdown';
        }

        $url = '';
        if( !empty( $item->url ) ) {
            $url = $item->url;
        }

        


        $target = '';
        if( !empty( $item->target ) ) {
            $target = 'target="'.$item->target.'"';
        }


        if (! is_array($url) ) {
            if ($item->menu_order == 1) {
                $output .= '
                <a class="h6 collapsed" data-toggle="collapse" aria-expanded="true"  href="#footerNav_'.$item->ID.'">' . $item->title . '</a>

                <div class="collapse show" id="footerNav_'.$item->ID.'">';
            } else {
                $output .= '
                <a class="nav-link" href="' . $url . '" '. $target .'>
                    ' . $item->title . '
                </a>';
            }

            if ( ($item->menu_order && $args->menu->count) && ($item->menu_order == $args->menu->count)) {
                $output .= '</div>';
            }
        }
    }

    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
        $output .= '';
    }
}

/**
 * LE WALKER
 */
class Le_Walker_Footer_2 extends Walker_Nav_Menu {
    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $classes = array();
        if( !empty( $item->classes ) ) {
            $classes = (array) $item->classes;
        }

        $parent_class = '';
        if( in_array('menu-item-has-children', $classes) ) {
            $parent_class = 'dropdown';
        }

        $url = '';
        if( !empty( $item->url ) ) {
            $url = $item->url;
        }

        $output .= '
        <a class="nav-link" href="' . $url . '">
            ' . $item->title . '
        </a>';
    }

    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
        $output .= '';
    }
}

// CATEGORY CLASS
class Walker_Category_Class extends Walker_Category {
    function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
        extract($args);

        $cat_name = esc_attr( $category->name );
        $cat_name = apply_filters( 'list_cats', $cat_name, $category );
        $link = '<a href="' . esc_url( get_term_link($category) ) . '" ';
        if ( $use_desc_for_title == 0 || empty($category->description) )
            $link .= 'title="' . esc_attr( sprintf(__( 'View all posts filed under %s' ), $cat_name) ) . '"';
        else
            $link .= 'title="' . esc_attr( strip_tags( apply_filters( 'category_description', $category->description, $category ) ) ) . '"';
            $link .= '>';
            $link .= $cat_name . '</a>';

        if ( !empty($show_count) )
            $link .= ' (' . intval($category->count) . ')';

            if ( 'list' == $args['style'] ) {
                    $output .= "\t<li";
                    $class = 'list-inline-item cat-item cat-item-' . $category->term_id;

                    $termchildren = get_term_children( $category->term_id, $category->taxonomy );
                    // add children class if you want to
                    if(count($termchildren)>0){
                        $class .=  ' ';
                    }

                    if ( !empty($current_category) ) {
                            $_current_category = get_term( $current_category, $category->taxonomy );
                            if ( $category->term_id == $current_category )
                                    $class .=  ' current-cat active';
                            elseif ( $category->term_id == $_current_category->parent )
                                    $class .=  ' current-cat-parent';
                    }

                    $output .=  ' class="' . $class . '"';
                    $output .= ">$link\n";
            } else {
                    $output .= "\t$link<br />\n";
            }
    }
}

/**
 * REMOVE SCRIPT VERSION
 */
function _remove_script_version( $src ){
    $parts = explode( '?ver', $src );
    return $parts[0];
}
// add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
// add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

// IMAGE SIZES
if ( function_exists( 'add_image_size' ) ) { 
    add_image_size( 'blog', 544, 336, array('top', 'center')); // crop image
    add_image_size( 'callout', 520); // callout annd gallery

    add_image_size( 'header', 2880);

    add_image_size( 'fl-left', 200); // TODO, reduce to one and rename
    add_image_size( 'slidernav', 300); // slidernav and sponsors and team

    add_image_size( 'slidercontent', 600);

    add_image_size( 'image', 1168);

    add_image_size( 'image-small', 768); // use the 800 instead
    add_image_size( 'fl-top', 800);
}

/**
 * REMOVE EMOJI
 */
function remove_emoji()
    {
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    add_filter('tiny_mce_plugins', 'remove_tinymce_emoji');
    }
add_action('init', 'remove_emoji');
function remove_tinymce_emoji($plugins)
    {
    if (!is_array($plugins))
        {
        return array();
        }
    return array_diff($plugins, array(
        'wpemoji'
    ));
}


/**
 * WP Bootstrap Navwalker
 *
 * @package WP-Bootstrap-Navwalker
 */

/*
 * Class Name: WP_Bootstrap_Navwalker
 * Plugin Name: WP Bootstrap Navwalker
 * Plugin URI:  https://github.com/wp-bootstrap/wp-bootstrap-navwalker
 * Description: A custom WordPress nav walker class to implement the Bootstrap 3 navigation style in a custom theme using the WordPress built in menu manager.
 * Author: Edward McIntyre - @twittem, WP Bootstrap, William Patton - @pattonwebz
 * Version: 3.0.3
 * Author URI: https://github.com/wp-bootstrap
 * GitHub Plugin URI: https://github.com/wp-bootstrap/wp-bootstrap-navwalker
 * GitHub Branch: master
 * License: GPL-3.0+
 * License URI: http://www.gnu.org/licenses/gpl-3.0.txt
*/

/* Check if Class Exists. */
if ( ! class_exists( 'WP_Bootstrap_Navwalker' ) ) {
    /**
     * WP_Bootstrap_Navwalker class.
     *
     * @extends Walker_Nav_Menu
     */
    class WP_Bootstrap_Navwalker extends Walker_Nav_Menu {

        /**
         * Start Level.
         *
         * @see Walker::start_lvl()
         * @since 3.0.0
         *
         * @access public
         * @param mixed $output Passed by reference. Used to append additional content.
         * @param int   $depth (default: 0) Depth of page. Used for padding.
         * @param array $args (default: array()) Arguments.
         * @return void
         */
        public function start_lvl( &$output, $depth = 0, $args = array() ) {
            $indent = str_repeat( "\t", $depth );
            // find all links with an id in the output.
            preg_match_all( '/(<a.*?id=\"|\')(.*?)\"|\'.*?>/im', $output, $matches );
            // with pointer at end of array check if we got an ID match.
            if ( end( $matches[2] ) ) {
                // build a string to use as aria-labelledby.
                $labledby = 'aria-labelledby="' . end( $matches[2] ) . '"';
            }

            $output .= "\n$indent<ul role=\"menu\" class=\" dropdown-menu\" " . $labledby . ">\n";
        }

        /**
         * Start El.
         *
         * @see Walker::start_el()
         * @since 3.0.0
         *
         * @access public
         * @param mixed $output Passed by reference. Used to append additional content.
         * @param mixed $item Menu item data object.
         * @param int   $depth (default: 0) Depth of menu item. Used for padding.
         * @param array $args (default: array()) Arguments.
         * @param int   $id (default: 0) Menu item ID.
         * @return void
         */
        public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
            $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

            $value = '';
            $class_names = $value;
            $classes = empty( $item->classes ) ? array() : (array) $item->classes;
            // Loop through the array and pick out any special classes that need
            // to be added to an element other than the main <li>.
            $extra_link_classes = array();
            $icon_class_string = '';
            foreach ( $classes as $key => $class ) {
                // test if this is a disabled link.
                if ( 'disabled' === $class ) {
                    $extra_link_classes[] = 'disabled';
                    unset( $classes[ $key ] );
                }

                if ( 'github-button' === $class ) {
                    $extra_link_classes[] = 'github-button';
                    unset( $classes[ $key ] );
                }

                // test for icon classes - Supports Font Awesome and Glyphicons.
                if ( 'fa' === $class || 'fa-' === substr( $class, 0, 3 ) ) {
                    // Because of the abiguity of just 'fa' at the start both
                    // 'fa' & 'fa-' are tested for with Font Awesome icons.
                    $icon_class_string .= $class . ' ';
                    unset( $classes[ $key ] );
                } elseif ( 'glyphicons' === substr( $class, 0, 10 ) ) {
                    // This should be a glyphicon icon class.
                    $icon_class_string .= $class . ' ';
                    unset( $classes[ $key ] );
                }
            }
            $classes[] = 'menu-item-' . $item->ID;
            // BSv4 classname - as of v4-alpha.
            $classes[] = 'nav-item';
            // reasign any filtered classes back to the $classes array.
            $classes = apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args );
            $class_names = join( ' ', $classes );
            if ( $args->has_children ) {
                $class_names .= ' dropdown';
            }
            if ( in_array( 'current-menu-item', $classes, true ) || in_array( 'current-menu-parent', $classes, true ) ) {
                $class_names .= ' active';
            }
            $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
            $id = apply_filters( 'nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args );
            $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
            $output .= $indent . '<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement"' . $id . $value . $class_names . '>';
            $atts = array();

            if ( empty( $item->attr_title ) ) {
                $atts['title']  = ! empty( $item->title )   ? strip_tags( $item->title ) : '';
            } else {
                $atts['title'] = $item->attr_title;
            }

            $atts['target'] = ! empty( $item->target )  ? $item->target : '';
            $atts['rel']    = ! empty( $item->xfn )     ? $item->xfn    : '';
            // If item has_children add atts to a.
            if ( $args->has_children && 0 === $depth && $args->depth > 1 ) {
                // $atts['href']           = '#';
                $atts['href']   = ! empty( $item->url ) ? $item->url : '';
                $atts['data-toggle']    = 'dropdown';
                $atts['aria-haspopup']  = 'true';
                $atts['aria-expanded']  = 'false';
                $atts['class']          = 'dropdown-toggle nav-link';
                $atts['id']             = 'menu-item-dropdown-' . $item->ID;
            } else {
                $atts['href']   = ! empty( $item->url ) ? $item->url : '';
                // if we are in a dropdown then the the class .dropdown-item
                // should be used instead of .nav-link.
                if ( $depth > 0 ) {
                    $atts['class']  = 'dropdown-item';
                } else {
                    $atts['class']  = 'nav-link';
                }
            }
            // Loop through the array of extra link classes plucked from the
            // parent <li>s classes array.
            if ( ! empty( $extra_link_classes ) ) {
                foreach ( $extra_link_classes as $link_class ) {

                    if ( ! empty( $link_class ) ) {
                        // update $atts with the extra class link.
                        $atts['class'] .= ' ' . esc_attr( $link_class );

                        // if the modification is a disabled class...
                        if ( 'disabled' === $link_class ) {
                            // then # the link so it doesn't point anywhere.
                            // $atts['href'] = '#';
                        }
                    }
                }
            }
            $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );
            $attributes = '';
            foreach ( $atts as $attr => $value ) {
                if ( ! empty( $value ) ) {
                    $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                    $attributes .= ' ' . $attr . '="' . $value . '"';
                }
            }
            $item_output = $args->before;
            $item_output .= '<a' . $attributes . '>';

            // initiate empty icon var then if we have a string containing icon classes...
            $icon_html = '';
            if ( ! empty( $icon_class_string ) ) {
                // append an <i> with the icon classes to what is output before links.
                $icon_html = '<i class="' . esc_attr( $icon_class_string ) . '" aria-hidden="true"></i> ';
            }
            $item_output .= $args->link_before . $icon_html . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
            $item_output .= '</a>';
            $item_output .= $args->after;
            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

        }

        /**
         * Traverse elements to create list from elements.
         *
         * Display one element if the element doesn't have any children otherwise,
         * display the element and its children. Will only traverse up to the max
         * depth and no ignore elements under that depth.
         *
         * This method shouldn't be called directly, use the walk() method instead.
         *
         * @see Walker::start_el()
         * @since 2.5.0
         *
         * @access public
         * @param mixed $element Data object.
         * @param mixed $children_elements List of elements to continue traversing.
         * @param mixed $max_depth Max depth to traverse.
         * @param mixed $depth Depth of current element.
         * @param mixed $args Arguments.
         * @param mixed $output Passed by reference. Used to append additional content.
         * @return null Null on failure with no changes to parameters.
         */
        public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
            if ( ! $element ) {
                return; }
            $id_field = $this->db_fields['id'];
            // Display this element.
            if ( is_object( $args[0] ) ) {
                $args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] ); }
            parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
        }

        /**
         * Menu Fallback
         * =============
         * If this function is assigned to the wp_nav_menu's fallback_cb variable
         * and a menu has not been assigned to the theme location in the WordPress
         * menu manager the function with display nothing to a non-logged in user,
         * and will add a link to the WordPress menu manager if logged in as an admin.
         *
         * @param array $args passed from the wp_nav_menu function.
         */
        public static function fallback( $args ) {
            if ( current_user_can( 'edit_theme_options' ) ) {

                /* Get Arguments. */
                $container = $args['container'];
                $container_id = $args['container_id'];
                $container_class = $args['container_class'];
                $menu_class = $args['menu_class'];
                $menu_id = $args['menu_id'];

                // initialize var to store fallback html.
                $fallback_output = '';

                if ( $container ) {
                    $fallback_output = '<' . esc_attr( $container );
                    if ( $container_id ) {
                        $fallback_output = ' id="' . esc_attr( $container_id ) . '"';
                    }
                    if ( $container_class ) {
                        $fallback_output = ' class="' . sanitize_html_class( $container_class ) . '"';
                    }
                    $fallback_output = '>';
                }
                $fallback_output = '<ul';
                if ( $menu_id ) {
                    $fallback_output = ' id="' . esc_attr( $menu_id ) . '"'; }
                if ( $menu_class ) {
                    $fallback_output = ' class="' . esc_attr( $menu_class ) . '"'; }
                $fallback_output = '>';
                $fallback_output = '<li><a href="' . esc_url( admin_url( 'nav-menus.php' ) ) . '" title="">' . esc_attr( 'Add a menu', '' ) . '</a></li>';
                $fallback_output = '</ul>';
                if ( $container ) {
                    $fallback_output = '</' . esc_attr( $container ) . '>';
                }

                // if $args has 'echo' key and it's true echo, otherwise return.
                if ( array_key_exists( 'echo', $args ) && $args['echo'] ) {
                    echo $fallback_output; // WPCS: XSS OK.
                } else {
                    return $fallback_output;
                }
            } // End if().
        }
    }
} // End if().

/**
 * ADD SVG SUPPORT
 */
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


/**
 * Plugin Name: Disable ACF on Frontend
 * Description: Provides a performance boost if ACF frontend functions aren't being used
 * Version:     1.0
 * Author:      Bill Erickson
 * Author URI:  http://www.billerickson.net
 * License:     MIT
 * License URI: http://www.opensource.org/licenses/mit-license.php
 */
 
/**
 * Disable ACF on Frontend
 *
 */
function ea_disable_acf_on_frontend( $plugins ) {

    if( is_admin() )
        return $plugins;

    foreach( $plugins as $i => $plugin )
        if( 'advanced-custom-fields-pro/acf.php' == $plugin )
            unset( $plugins[$i] );
    return $plugins;
}
add_filter( 'option_active_plugins', 'ea_disable_acf_on_frontend' );

/**
 * Returns the ACF JSON filename by group name.
 *
 * @param string $field_group_name Name of the ACF field group
 * @return string|bool
 */
function get_acf_json_by_name( string $field_group_name ) {
    $field_group_lookup = [
        'sections' => 'group_5a0031f33b316.json',
        'header'  => 'group_5a015fa29e85a.json',
        'pricing' => 'group_5a1bc411e4057.json',
        'post' => 'group_5a1d70cfe90a0.json',
        'press' => 'group_5a5dccf9e5d41.json',
        'overview' => 'group_5a27f85666042.json',
        'external' => 'group_5a2ff8b3a9b93.json',
        'events' => 'group_5a703e597f559.json',
        'intercom' => 'group_5ace209e4e0b0.json',
        'subnav' => 'group_5afac0dd3ac1e.json',
        'scripts' => 'group_5b1580e8603a7.json',
        'header_getcrate' => 'group_5bd72f5c8cda5.json',
        // Add more field_group_name => file_name pairs here.
    ];
    if ( ! isset( $field_group_lookup[ $field_group_name ] ) ) {
        return false;
    }
    return $field_group_lookup[ $field_group_name ];
}


function set_acf_conf( string $field_group, bool $option = false, $custom_id = '' ) {
    $field_group_json = get_acf_json_by_name( $field_group );
    $field_group_file = get_stylesheet_directory() . "/acf-json/{$field_group_json}";

    if (file_exists($field_group_file)) {
      $field_group_array = json_decode( file_get_contents( $field_group_file ), true );
      $config = $field_group_array['fields'];
    }

    if ( !empty($config) ) {
        if ($option) {
            return get_all_custom_field_meta( 'option', $config );
        }

        if ($custom_id) {
            return get_all_custom_field_meta( $custom_id, $config );
        } else {
            return get_all_custom_field_meta( get_the_ID(), $config );
        }
    }
}


/**
 * Breadcrumb
 */
function nav_breadcrumb() {

    $delimiter = '<span>&gt;</span>';
    $home = 'Home'; 
    $before = '<span class="current-page">'; 
    $after = '</span>'; 

    if ( is_home() ||  !is_front_page() || is_paged() ) {
        echo '<nav class="cr-tag-breadcrumb">';

        global $post;
        $homeLink = get_bloginfo('url');
        echo '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';

        if ( is_category()) {
            global $wp_query;
            $cat_obj = $wp_query->get_queried_object();
            $thisCat = $cat_obj->term_id;
            $thisCat = get_category($thisCat);
            $parentCat = get_category($thisCat->parent);
            if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
            echo $before . single_cat_title('', false) . $after;

        } elseif ( is_day() ) {
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
            echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
            echo $before . get_the_time('d') . $after;

        } elseif ( is_month() ) {
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
            echo $before . get_the_time('F') . $after;

        } elseif ( is_year() ) {
            echo $before . get_the_time('Y') . $after;

        } elseif ( is_home() ) {
            echo $before . get_the_title(get_option( 'page_for_posts' )) . $after;

        } elseif ( is_single() && !is_attachment() ) {
            if ( get_post_type() != 'post' ) {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';
                echo $before . get_the_title() . $after;
            } else {
                $cat = get_the_category(); $cat = $cat[0];
                // set breadcrumb to blogpage in options instead of category
                
                if (get_post_type() == 'post') {
                    if ( get_option( 'page_for_posts' ) ) {
                    echo '<a href="'.get_post_type_archive_link( 'post' ).'">'.esc_html__( 'Blog', 'textdomain' ).'</a> ' . $delimiter;
                    } else {
                        echo '<a href="/blog">'.esc_html__( 'Blog', 'textdomain' ).'</a> ' . $delimiter . ' ';
                    }
                } else {
                    echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
                }
                
                echo $before . get_the_title() . $after;
            }

        } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
            if (get_post_type()) {
                $post_type = get_post_type_object(get_post_type());
                echo $before . $post_type->labels->singular_name . $after;
            }
        } elseif ( is_attachment() ) {
            $parent = get_post($post->post_parent);
            $cat = get_the_category($parent->ID); $cat = $cat[0];
            echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
            echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
            echo $before . get_the_title() . $after;

        } elseif ( is_page() && !$post->post_parent ) {
            echo $before . get_the_title() . $after;

        } elseif ( is_page() && $post->post_parent ) {
            $parent_id = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page = get_page($parent_id);
                $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
                $parent_id = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
            echo $before . get_the_title() . $after;

        } elseif ( is_search() ) {
            echo $before . 'Search results for "' . get_search_query() . '"' . $after;

        } elseif ( is_tag() ) {
            echo $before . 'Posts with the tag "' . single_tag_title('', false) . '"' . $after;

        } elseif ( is_404() ) {
            echo $before . 'Error 404' . $after;
        }

        // if ( get_query_var('paged') ) {
        //     if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
        //     echo ': ' . __('Seite') . ' ' . get_query_var('paged');
        //     if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
        // }

        echo '</nav>';

    } 
} 


function get_archives_link_mod ( $link_html ) {
    preg_match ("/value='(.+?)'/", $link_html, $url);
    $requested = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if ($requested == $url[1]) {
        $link_html = str_replace("<option", "<option selected", $link_html);
    }
    return $link_html;
}


/**
 * ADVANCED CUSTOM FIELDS FLEXIBLE CONTENT TITLE
 */

function my_acf_flexible_content_layout_title( $title, $field, $layout, $i ) {
    // remove layout title from text
    $boxTitle = '';

    // Type
    $boxTitle .= ' <span style="text-transform:uppercase;font-weight: bold;">' . $title . '</span>';

    // Tag + Title
    $sectionSubTitle = get_sub_field('subtitle') ?? '';
    $sectionTitle = get_sub_field('title') ?? '';
    $sectionDescription = get_sub_field('description') ?? '';

    if( !empty($sectionSubTitle) ) {
        $boxTitle .=  ' | ' . '<span style="color: blue;">' . $sectionSubTitle . '</span>';
    }

    if( !empty($sectionTitle) ) {
        $boxTitle .=  ' | ' . '<span style="color: blue;">' . $sectionTitle . '</span>';
    }

    if( !empty($sectionDescription) ) {
        $boxTitle .=  ' | ' . '<span style="color: blue;">' . substr(strip_tags($sectionDescription), 0, 12) . '...</span>';
    }

    // IMAGE
    $sectionImage = get_sub_field('image')['sizes']['thumbnail'] ?? '';
    if ( !empty($sectionImage) ) {
        $sectionImage = '<div style="padding-top: 5px;"><img style="max-width: 50px;" src="'.$sectionImage.'" alt=""></div>';

        $boxTitle .= $sectionImage;
    }

    // return
    return $boxTitle;
}

// name
add_filter('acf/fields/flexible_content/layout_title', 'my_acf_flexible_content_layout_title', 10, 4);



// REMOVE ARCHIVE TITLE
add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});


require_once('wp_bootstrap_pagination.php');


/**
 * BOOTSTRAP PAGINATION
 */
function customize_wp_bootstrap_pagination($args) {
    
    $args['previous_string'] = 'previous';
    $args['next_string'] = 'next';
    
    return $args;
}
add_filter('wp_bootstrap_pagination_defaults', 'customize_wp_bootstrap_pagination');


add_filter( 'get_shortlink', function( $shortlink ) {return $shortlink;} );


function disable_search( $query, $error = true ) {
  if ( is_search() ) {
    $query->is_search = false;
    // $query->query_vars[s] = false;
    // $query->query[s] = false;
    // to error
    if ( $error == true )
    $query->is_404 = true;
  }
}

add_action( 'parse_query', 'disable_search' );
// add_filter( 
//     'get_search_form', create_function( '$a', "return null;" ) 
// );
add_filter( 
    'get_search_form', function($a) {return null;}
);


 




/**
 * Defines alternative titles for various event views.
 *
 * @param  string $title
 * @return string
 */
function filter_events_title( $title ) {
    // Single events
    if ( tribe_is_event() && is_single() ) {
        // $title = 'Single event page';
    }
    // Single venues
    // elseif ( tribe_is_venue() ) {
    //     $title = 'Single venue page';
    // }
    // Single organizers
    // elseif ( tribe_is_organizer() && is_single() ) {
    //     $title = 'Single organizer page';
    // }
    // Month view Page
    // elseif ( tribe_is_month() && !is_tax() ) {
    //     $title = 'Month view page';
    // }
    // Month view category page
    // elseif ( tribe_is_month() && is_tax() ) {
    //     $title = 'Month view category page';
    // }
    // List view page: upcoming events
    elseif ( tribe_is_upcoming() && ! is_tax() ) {
        $title = 'CrateDB Events: Upcoming Events';
    }
    // List view category page: upcoming events
    // elseif ( tribe_is_upcoming() && is_tax() ) {
    //     $title = 'List view category: upcoming events page';
    // }
    // List view page: past events
    elseif ( tribe_is_past() && !is_tax() ) {
        $title = 'CrateDB Events: Past Events';
    }
    // List view category page: past events
    // elseif ( tribe_is_past() && is_tax() ) {
    //     $title = 'List view category: past events page';
    // }
    // Week view page
    // elseif ( tribe_is_week() && ! is_tax() ) {
    //     $title = 'Week view page';
    // }
    // Week view category page
    // elseif ( tribe_is_week() && is_tax() ) {
    //     $title = 'Week view category page';
    // }
    // Day view page
    elseif ( tribe_is_day() && ! is_tax() ) {
        $title = 'Day view page';
    }
    // Day view category page
    elseif ( tribe_is_day() && is_tax() ) {
        $title = 'Day view category page';
    }
    // Map view page
    // elseif ( tribe_is_map() && ! is_tax() ) {
    //     $title = 'Map view page';
    // }
    // Map view category page
    // elseif ( tribe_is_map() && is_tax() ) {
    //     $title = 'Map view category page';
    // }
    // Photo view page
    // elseif ( tribe_is_photo() && ! is_tax() ) {
    //     $title = 'Photo view page';
    // }
    // Photo view category page
    // elseif ( tribe_is_photo() && is_tax() ) {
    //     $title = 'Photo view category page';
    // }
    return $title;
}
/**
 * Modifes the event <title> element.
 *
 * Users of Yoast's SEO plugin may wish to try replacing the below line with:
 *
 *     add_filter('wpseo_title', 'filter_events_title' );
 */
// add_filter( 'tribe_events_title_tag', 'filter_events_title' );
add_filter('wpseo_title', 'filter_events_title' );



/* Add External Sitemap to Yoast Sitemap Index
 * Credit: Paul https://wordpress.org/support/users/paulmighty/
 * Last Tested: Aug 25 2017 using Yoast SEO 5.3.2 on WordPress 4.8.1
 *********
 * This code adds two external sitemaps and must be modified before using.
 * Replace http://www.example.com/external-sitemap-#.xml
   with your external sitemap URL.
 * Replace 2018-01-30T23:12:27+00:00
   with the time and date your external sitemap was last updated.
   Format: yyyy-MM-dd'T'HH:mm:ssZ
 * If you have more/less sitemaps, add/remove the additional section.
 *********
 * Please note that changes will be applied upon next sitemap update.
 * To manually refresh the sitemap, please disable and enable the sitemaps.
 */
add_filter( 'wpseo_sitemap_index', 'add_sitemap_custom_items' );
function add_sitemap_custom_items() {
   $sitemap_custom_items = '
<sitemap>
    <loc>https://crate.io/docs/clients/python/en/latest/sitemap.xml</loc>
    <lastmod>2018-02-03T21:28:27+00:00</lastmod>
</sitemap>
<sitemap>
    <loc>https://crate.io/docs/clients/pdo/en/latest/sitemap.xml</loc>
    <lastmod>2018-02-03T21:28:27+00:00</lastmod>
</sitemap>
<sitemap>
    <loc>https://crate.io/docs/clients/jdbc/en/latest/sitemap.xml</loc>
    <lastmod>2018-02-03T21:28:27+00:00</lastmod>
</sitemap>
<sitemap>
    <loc>https://crate.io/docs/crate/guide/en/latest/sitemap.xml</loc>
    <lastmod>2018-02-03T21:28:27+00:00</lastmod>
</sitemap>
<sitemap>
    <loc>https://crate.io/docs/crate/getting-started/en/latest/sitemap.xml</loc>
    <lastmod>2018-02-03T21:28:27+00:00</lastmod>
</sitemap>
<sitemap>
    <loc>https://crate.io/docs/clients/dbal/en/latest/sitemap.xml</loc>
    <lastmod>2018-02-03T21:28:27+00:00</lastmod>
</sitemap>
<sitemap>
    <loc>https://crate.io/docs/clients/crash/en/latest/sitemap.xml</loc>
    <lastmod>2018-02-03T21:28:27+00:00</lastmod>
</sitemap>
<sitemap>
    <loc>https://crate.io/docs/crate/reference/en/latest/sitemap.xml</loc>
    <lastmod>2018-02-03T21:28:27+00:00</lastmod>
</sitemap>
<sitemap>
    <loc>https://crate.io/docs/sql-99/en/latest/sitemap.xml</loc>
    <lastmod>2018-02-03T21:28:27+00:00</lastmod>
</sitemap>


';

/* DO NOT REMOVE ANYTHING BELOW THIS LINE
 * Send the information to Yoast SEO
 */
return $sitemap_custom_items;
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
    require get_template_directory() . '/inc/jetpack.php';
}

