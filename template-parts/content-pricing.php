<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crate
 */

    // $post_id = get_the_ID();
 //    $posts = get_post_meta( $post_id );
 //    echo '<pre>';
 //    print_r($posts);
 //    echo '</pre>';

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <!-- HEADER -->
    <?php include(locate_template('partials/header.php')); ?>

    <!-- SECTIONS -->
    <?php include(locate_template('partials/pricing.php')); ?>

    <?php 
        if (function_exists('set_acf_conf')) { 
            $meta_data = set_acf_conf('sections'); 
        }

        if ( !empty($meta_data) ) {
            foreach ($meta_data as $key => $meta) {
                // IF ARRAY
                if ( is_array($meta) ) {
                    foreach ($meta as $layout) {
                        if (file_exists(get_template_directory() . '/partials/'.$layout['acf_fc_layout'].'.php')) {
                            include(locate_template('partials/'.$layout['acf_fc_layout'].'.php'));
                        }
                    }
                }
            }
        }
    ?>

    <!-- SOCIAL -->
    <div></div>
</article><!-- #post-<?php the_ID(); ?> -->
