<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crate
 */

    // $post_id = get_the_ID();
 //    $posts = get_post_meta( $post_id );
 //    echo '<pre>';
 //    print_r($posts);
 //    echo '</pre>';
    $template = get_post_meta( get_the_id(), '_wp_page_template', true ) ?? '';
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <!-- HEADER -->
    <?php require_once(locate_template('partials/header.php')); ?>


    <?php if ($template == 'page-thankyou.php') : ?>

        <?php 
            if (file_exists(get_template_directory() . '/partials/thankyou.php') ) {
                include(locate_template('partials/thankyou.php'));
            }
        ?>
    <?php endif; ?>

    <?php if ($template == 'page-license.php') : ?>

        <?php 
            if (file_exists(get_template_directory() . '/partials/license.php') ) {
                include(locate_template('partials/license.php'));
            }
        ?>
    <?php endif; ?>

    <?php
        // POST DETAIL
        if ((get_post_type() == 'post') && (file_exists(get_template_directory() . '/partials/post_detail.php')) ) {
            include(locate_template('partials/post_detail.php'));
        }
    ?>

    <?php
        // SECTIONS
        if (function_exists('set_acf_conf')) {
            $meta_data = set_acf_conf('sections');
        }
    ?>

    <?php if ( !empty($meta_data) ) : ?>
        <?php foreach ($meta_data as $key => $meta) : ?>
            <?php if ( is_array($meta) ) : ?>
                <?php foreach ($meta as $layout) : ?>
                    <?php if (file_exists(get_template_directory() . '/partials/' . $layout['acf_fc_layout'] . '.php')) : ?>
                        <?php include(locate_template('partials/' . $layout['acf_fc_layout'] . '.php')); ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>

    <!-- SOCIAL SHARE -->

    <?php 
    global $wp;
    ?>
<?php if (is_single()) : ?>
<div class="cr-section-share py-5">
  <div class="container">
    <div class="row justify-content-center">
      
      <div class="col-12 text-center">
        <ul class="cr-share-list list-inline mb-5">
          <li class="list-inline-item">
            Share
          </li>
          <li class="list-inline-item">
            <a target="_blank" rel="noopen" href="http://www.facebook.com/sharer/sharer.php?u=<?php echo home_url( $wp->request ); ?>">
              <svg width="20px" height="20px" viewBox="0 0 12 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <path d="M11.2456667,6.38458333 L7.33333333,6.38458333 L7.33333333,4.63833333 C7.33333333,3.817 7.87783333,3.62541667 8.261,3.62541667 L11,3.62541667 L11,0.0119166667 L7.03175,0 C3.43291667,0 2.75,2.69316667 2.75,4.41833333 L2.75,6.38458333 L0,6.38458333 L0,10.05125 L2.75,10.05125 L2.75,21.05125 L7.33333333,21.05125 L7.33333333,10.05125 L10.8643333,10.05125 L11.2456667,6.38458333 Z" id="icon-share-facebook" fill="#55D4F5"></path>
              </svg>
            </a>
          </li>
          <li class="list-inline-item">
            <a target="_blank" rel="noopen" href="https://twitter.com/intent/tweet?url=<?php echo home_url( $wp->request ); ?>">
              <svg width="20px" height="20px" viewBox="0 0 23 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <path d="M22.443,2.334 C21.63,2.697 20.943,2.709 20.215,2.35 C21.154,1.788 21.197,1.393 21.536,0.331 C20.658,0.852 19.685,1.231 18.65,1.435 C17.822,0.553 16.642,0 15.335,0 C12.825,0 10.791,2.036 10.791,4.544 C10.791,4.9 10.831,5.247 10.908,5.58 C7.131,5.391 3.783,3.582 1.542,0.832 C1.151,1.503 0.926,2.284 0.926,3.117 C0.926,4.694 1.73,6.084 2.948,6.899 C2.203,6.875 1.503,6.671 0.891,6.331 C0.89,6.35 0.89,6.368 0.89,6.388 C0.89,8.59 2.456,10.426 4.536,10.844 C3.87,11.025 3.168,11.053 2.483,10.923 C3.062,12.727 4.74,14.041 6.728,14.078 C4.783,15.602 2.372,16.236 0,15.959 C2.012,17.248 4.399,18 6.965,18 C15.324,18 19.894,11.076 19.894,5.071 C19.894,4.873 19.89,4.678 19.881,4.483 C20.769,3.843 21.835,3.246 22.443,2.334" id="icon-share-twitter" fill="#55D4F5"></path>
              </svg>
            </a>
          </li>

          <li class="list-inline-item">
            <a target="_blank" rel="noopen" href="https://www.linkedin.com/shareArticle?url=<?php echo home_url( $wp->request ); ?>">
              <svg width="20px" height="20px" viewBox="0 0 22 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <path d="M14.027,10.893 C12.922,10.893 12.027,11.789 12.027,12.893 L12.027,19.893 L7.027,19.893 C7.027,19.893 7.086,7.893 7.027,6.893 L12.027,6.893 L12.027,8.378 C12.027,8.378 13.575,6.935 15.964,6.935 C18.927,6.935 21.027,9.079 21.027,13.239 L21.027,19.893 L16.027,19.893 L16.027,12.893 C16.027,11.789 15.131,10.893 14.027,10.893 L14.027,10.893 Z M2.518,4.893 L2.487,4.893 C0.978,4.893 0,3.711 0,2.412 C0,1.083 1.007,0 2.547,0 C4.088,0 5.035,1.118 5.066,2.447 C5.065,3.747 4.088,4.893 2.518,4.893 L2.518,4.893 Z M5.027,19.893 L0.027,19.893 L0.027,6.893 L5.027,6.893 L5.027,19.893 Z" id="icon-share-linkedin" fill="#55D4F5"></path>
              </svg>
            </a>
          </li>
          <li class="list-inline-item">
            <a target="_blank" rel="noopen" href="mailto:?subject=I wanted you to see this site <?php echo home_url( $wp->request ); ?>">
              <svg width="20px" height="20px" viewBox="0 0 22 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <path d="M21.708,1 C21.889,1.29948659 22,1.64689104 22,2.02224758 L22,13.0034227 C22,14.1045351 21.104,15 20,15 L2,15 C0.897,15 0,14.1045351 0,13.0034227 L0,2.02224758 C0,1.64689104 0.111,1.29948659 0.292,1 L10.685,9.3986024 C10.777,9.47247576 10.889,9.50941244 11,9.50941244     C11.111,9.50941244 11.223,9.47247576 11.314,9.3986024 L21.708,1 Z M21,0.259423238 L11,8 L1,0.259423238 C1.29335202,0.0995572574 1.6277533,0 1.98918702,0 L20.010813,0 C20.3722467,0 20.706648,0.0995572574 21,0.259423238 Z" id="icon-share-email" fill="#55D4F5"></path>
              </svg>
            </a>
          </li>

          <li class="list-inline-item">
            <a target="_blank" rel="noopen" href="http://www.reddit.com/submit?url=<?php echo home_url( $wp->request ); ?>">
              <svg width="24px" height="21px" viewBox="0 0 24 21" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <defs>
                      <polyline id="path-1" points="0 0 24 0 24 24 0 24"></polyline>
                  </defs>
                  <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <g id="Footer" transform="translate(-1287.000000, -176.000000)">
                          <g id="Group-9" transform="translate(1105.000000, 139.000000)">
                              <g id="Social-Logos-Copy" transform="translate(0.000000, 36.000000)">
                                  <g id="icon-follow-youtube-copy" transform="translate(182.000000, 0.000000)">
                                      <mask id="mask-2" fill="white">
                                          <use xlink:href="#path-1"></use>
                                      </mask>
                                      <g id="Clip-2"></g>
                                      <g id="logo-reddit" mask="url(#mask-2)" fill="#111111" fill-rule="nonzero">
                                          <g transform="translate(0.000000, 0.500000)" id="Shape">
                                              <path d="M24,10.5 C24,8.846 22.654,7.5 21,7.5 C20.036,7.5 19.137,7.976 18.578,8.741 C16.939,7.735 14.831,7.101 12.514,7.018 C12.578,5.908 12.914,3.969 14.022,3.332 C14.742,2.918 15.755,3.083 17.032,3.81 C17.1866437,5.33433989 18.4678424,6.49553411 20,6.5 C21.654,6.5 23,5.154 23,3.5 C23,1.846 21.654,0.5 20,0.5 C18.6490713,0.504489788 17.468254,1.41252726 17.117,2.717 C15.688,2 14.479,1.915 13.521,2.466 C11.879,3.411 11.57,5.943 11.513,7.017 C9.186,7.096 7.067,7.731 5.422,8.741 C4.85789935,7.96577644 3.95872602,7.5050522 3,7.5 C1.346,7.5 0,8.846 0,10.5 C0,11.819 0.836,12.943 2.047,13.344 C2.01720793,13.5614248 2.0015085,13.7805488 2,14 C2,17.86 6.486,21 12,21 C17.514,21 22,17.86 22,14 C21.9986211,13.7798445 21.9825856,13.560025 21.952,13.342 C23.1699164,12.9270416 23.9917119,11.7866399 24,10.5 Z M2.286,12.366 C1.50913013,12.07434 0.99601553,11.3298051 1,10.5 C1,9.397 1.897,8.5 3,8.5 C3.635,8.5 4.217,8.818 4.59,9.316 C3.488,10.17 2.683,11.211 2.286,12.366 Z M6,12.5 C6,11.397 6.897,10.5 8,10.5 C9.103,10.5 10,11.397 10,12.5 C10,13.603 9.103,14.5 8,14.5 C6.897,14.5 6,13.603 6,12.5 Z M15.787,17.314 C14.724,17.926 13.38,18.263 12,18.263 C10.613,18.263 9.263,17.923 8.197,17.305 C8.03707575,17.2182231 7.93691499,17.0514208 7.93547841,16.8694759 C7.93404182,16.6875311 8.03155621,16.5191679 8.19009027,16.4298766 C8.34862434,16.3405853 8.54314986,16.3444625 8.698,16.44 C10.526,17.499 13.456,17.502 15.288,16.448 C15.5260465,16.3208931 15.8220063,16.4056144 15.9567347,16.6394317 C16.0914632,16.8732491 16.0163442,17.1717906 15.787,17.314 Z M16,14.5 C14.897,14.5 14,13.603 14,12.5 C14,11.397 14.897,10.5 16,10.5 C17.103,10.5 18,11.397 18,12.5 C18,13.603 17.103,14.5 16,14.5 Z M21.713,12.365 C21.316,11.21 20.512,10.17 19.41,9.317 C19.7819255,8.80965782 20.3709723,8.50698412 21,8.5 C22.103,8.5 23,9.397 23,10.5 C23,11.335 22.468,12.073 21.713,12.365 Z"></path>
                                          </g>
                                      </g>
                                  </g>
                              </g>
                          </g>
                      </g>
                  </g>
              </svg>
            </a>
          </li>

        </ul>
      </div>

    </div>
  </div>
</div>
<?php endif; ?>

</article><!-- #post-<?php the_ID(); ?> -->
