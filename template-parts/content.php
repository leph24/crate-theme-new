<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crate
 */
?>

<?php 
    if (file_exists(get_template_directory() . '/partials/overview_item.php') ) {
        include (locate_template('partials/overview_item.php')); 
    }
?>