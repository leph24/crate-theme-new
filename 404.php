<?php
/**
* The template for displaying 404 pages (not found)
*
* @link https://codex.wordpress.org/Creating_an_Error_404_Page
*
* @package crate
*/

get_header(); ?>

<?php 
  $goats = array(
    'dzMIwSew2GVZ6', 
    '12m4TK1i8yopOw',
    'o8mcMybNKxEJ2',
    'H4J6OlQaLYjqo',
    '9rPANvP50RSWA',
    '1poweZ4nHn1r9XNMnF',
    '127yliXp6FmIuI',
    'rRi6pEYDkwjKM',
    '4UFaDWxzJIeHe',
    'l8aAwVtd7VPe8',
    'zz49TLnbLJdXq'
  );

  $random_goats = $goats[array_rand($goats)];
?>



<div class="cr-section-header-wrapper">
  <div class="cr-section-header cr-bg-graphic cr-bg-404" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/header-bg-404.png');">
    <div class="container">
      <div class="row">
        <header class="col">
          <?php if (function_exists('nav_breadcrumb')) nav_breadcrumb(); ?>
          <h1 class="cr-page-header"><?php esc_html_e( 'This thing your were looking for?', 'crate' ); ?></h1>
        </header>
      </div>
      <div class="row justify-content-center">
        <div class="col-md-8">
          <p class="lead"><?php esc_html_e( 'It doesn\'t exist here. Sorry.', 'crate' ); ?></p>
          <div class="cr-seperator"></div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="cr-section-content">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-8">
            <!--
            <div class="cr-quote">
              <p id="cr-quote" class="cr-quote-text">„We have to continually be jumping off cliffs and developing our wings on the way down.“</p>
              <span id="cr-quote-author">Kurt Vonnegut</span>
            </div>
            -->
            <div class="text-center">
              <p class="lead">
                But don't worry! You can go back to our <a href="<?php echo esc_url( home_url( '/' ) ); ?>">homepage</a> or look at this random animated goat gif for a while...
              </p>
              <p>

              <a href="https://giphy.com/gifs/<?php echo $random_goats; ?>">
                <img src="https://media.giphy.com/media/<?php echo $random_goats; ?>/giphy.gif" alt="404 image with a goat">
              </a>

<!-- 
                <a href="http://thecatapi.com">
                  <img alt="cat image" src="https://thecatapi.com/api/images/get?format=src&amp;type=gif">
                </a> -->




              </p>
              <p class="small">
                <a href="https://giphy.com" target="_blank" rel="nofollow" >
                  <img src="<?php echo get_template_directory_uri(); ?>/img/Poweredby_640px-White_HorizText.png" alt="powered by giphy" style="max-width: 120px;">
                </a>.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  <!-- END: Content -->



<?php
get_footer();