(function($) {
$(document).ready(function () {
  var open = false;
  var $root = $('html, body');

  new Clipboard('.codebut');
  $(".codebut").click(function()
    {
    this.innerHTML = "Copied";
    }
  );


  ScrollOut({
     targets: '.photo', 
     //once: true,
     onShown: function(el) {
        if (!el.src) { 
           el.src = el.dataset.src;
        }
     }
  })




  // Footer links collapse on mobile
  if (matchMedia) {
    const mq = window.matchMedia("(min-width: 768px)");
    mq.addListener(collapseFooterLinks);
    collapseFooterLinks(mq);
  }

  function collapseFooterLinks(mq){
    if(mq.matches){
      //console.log("window: big");
      $(".cr-section-footer div.collapse").addClass(" show")
      $(".cr-section-footer a.h6").addClass(" disabled");
      $(".cr-section-footer a.h6").removeClass("dropdown-toggle");

      $('.navbar-nav .dropdown').hover(function() {
        $(this).find('.dropdown-menu').first().stop(true, true).delay(150).slideDown(200);
      }, function() {
        $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp(200);
      });

      $('.navbar-nav .dropdown > a').click(function(){
        location.href = this.href;
      });

    } else {
      //console.log("window: small");
      $(".cr-section-footer div.collapse").removeClass("show");
      $(".cr-section-footer a.h6").removeClass("disabled");
      $(".cr-section-footer a.h6").addClass(" dropdown-toggle");
    }
  }

  //Navbar Toggle Animation
  $(".cr-navbar-toggle").click(function(){
    if (!open){
      $(".cr-navtoggle-top").css({
        'transition': 'transform 100ms',
        'transform': 'translateY(10px) rotateZ(45deg)'
      });
      $(".cr-navtoggle-mid").css({
        'transition': 'opacity',
        'opacity': '0'
      });
      $(".cr-navtoggle-bottom").css({
        'transition': 'transform 100ms',
        'transform': 'translateY(-10px) rotateZ(-45deg)'
      });
      open = true;
    }else{
      $(".cr-navtoggle-top").css({
        'transition': 'transform 100ms',
        'transform': 'translateY(0) rotateZ(0)'
      });
      $(".cr-navtoggle-mid").css({
        'transition': 'opacity',
        'opacity': '1'
      });
      $(".cr-navtoggle-bottom").css({
        'transition': 'transform 100ms',
        'transform': 'translateY(0) rotateZ(0)'
      });
      open = false;
    }
  });

  // Pricing table mobile view
  $("#pricing-basecamp").click(function(){
    $("#pricing-summit").removeClass("active");
    $("#pricing-service").removeClass("active");
    $(this).addClass("active");
    $(".cr-pricing-1").addClass("show");
    $(".cr-pricing-2").removeClass("show");
    $(".cr-pricing-3").removeClass("show");
  });

  $("#pricing-summit").click(function(){
    $("#pricing-basecamp").removeClass("active");
    $("#pricing-service").removeClass("active");
    $(this).addClass("active");
    $(".cr-pricing-1").removeClass("show");
    $(".cr-pricing-2").addClass("show");
    $(".cr-pricing-3").removeClass("show");
    //console.log("summit");
  });

  $("#pricing-service").click(function(){
    $("#pricing-basecamp").removeClass("active");
    $("#pricing-summit").removeClass("active");
    $(this).addClass("active");
    $(".cr-pricing-1").removeClass("show");
    $(".cr-pricing-2").removeClass("show");
    $(".cr-pricing-3").addClass("show");
  });

  $('.logo-slider').slick({
    infinite: false,
    dots: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }
      ,
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      }

    ]
  });

  $('.photo-slider').slick({
    infinite: false,
    dots: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }
      ,
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      }

    ]
  });

  $('.slider-nav').slick({
    infinite: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      }
      ,
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1
        }
      }

    ]
  });

  $('.slider-for').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav',
    adaptiveHeight: true
  });

  $(".cr-nav-cloud .dropdown-menu a").click(function(){
     $(".dropdown-val").text($(this).text());
     $(".dropdown-val").val($(this).text());

     if($(".dropdown-val").text() != "All"){
       $(this).parent().parent().addClass("active");
     }else if($(".dropdown-val").text() == "All"){
      // console.log("remove");
       $(this).parent().parent().removeClass("active");
     }
  });

  // Javascript to enable link to tab
  var hash = document.location.hash;
  var prefix = "tab_";
  if (hash) {
      $('#downloadOptions a[href="'+hash.replace(prefix,"")+'"]').tab('show');
  }

  // Change hash for page-reload
  // $('#downloadOptions a').on('shown', function (e) {
  //     window.location.hash = e.target.hash.replace("#", "#" + prefix);
  // });

  $('#downloadOptions a').on('shown.bs.tab', function(event){
    if(history.pushState) {
      history.pushState(null, null, event.target.hash.replace("/#", "#" + prefix));
    }
    else {
        location.hash = event.target.hash.replace("/#", "#" + prefix);
    }
  });

  
  $(".cr-imageGallery").lightGallery({
    selector: '.cr-img-single',
    thumbnail: false,
  });

  $(".cr-videoGallery").lightGallery({
    selector: 'a',
    loadYoutubeThumbnail: true,
    youtubeThumbSize: 'default',
    loadVimeoThumbnail: true,
    vimeoThumbSize: 'thumbnail_medium'
  });



});
})( jQuery );




window.addEventListener("load", function(){
  window.cookieconsent.initialise({
    "palette": {
      "popup": {
        "background": "#000",
        "text": "#ababab"
      },
      "button": {
        "background": "#55d4f5"
      },
      "highlight": {
        "background": "#79e7ff"
      }
    },
    "content": {
      "message": "This website uses cookies to ensure you get the best experience on our website.",
      "href": "https://crate.io/legal/privacy-policy/"
    }
  })
});