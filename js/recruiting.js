(function($) {
  var offersList;

  var listOffers = function() {
    $.getJSON("https://api.recruitee.com/c/crateio/careers/offers/?scope=active", function(data) {
      var container = $('');
      var output = $('<div class="cr-feature-list cr-feature-list--small col-md-8"></div>');

      for (var i in data.offers) {
        var offer_url = 'https://jobs.crate.io/o/'+data.offers[i].slug;
        var offer_department = '<span class="cr-tag">' + data.offers[i].department + '</span>';
        var offer_title = '<h6><a href="' + offer_url + '">' + data.offers[i].title + '</a></h6>';
        var offer_location = '<p>' + data.offers[i].location + '</p>';


        var offer = $('<div class="cr-feature"><div class="cr-feature-body d-flex"><div class="pt-1 mr-4"><svg width="34px" height="19px" viewBox="0 0 34 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path transform="translate(-336.000000, -809.000000)" fill-rule="nonzero" fill="#55D4F5" d="M366.085786,819.414214 L336,819.414214 L336,817.414214 L366.085786,817.414214 L359.085786,810.414214 L360.5,809 L369.914214,818.414214 L369.207107,819.12132 L360.5,827.828427 L359.085786,826.414214 L366.085786,819.414214 Z"></path></svg></div><div>'+ offer_department + offer_title + offer_location +'</div></div></div>');

          output.append(offer);
      }

      $("#grnhse_app").append(output);


      //render jobs list
      // for (var i in data.offers){

      //   var department = $("<div class='row justify-content-center'>"+"<div class='cr-feature-list cr-feature-list--small col-md-8'><h3 class='cr-department-title'>"+ offersList[i].department +"</h3></div>"+"</div>");
      //   $("#grnhse_app").append(department);

      //   for (var index in offersList[i].offers){
      //     var offer = $('<div class="cr-feature"><div class="cr-feature-body d-flex"></div></div>');
      //     var offer_url = "https://jobs.crate.io/o/"+offersList[i].offers[index].slug;

      //     var offer_department = $('<div class="pt-1 mr-4"><svg width="34px" height="19px" viewBox="0 0 34 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path transform="translate(-336.000000, -809.000000)" fill-rule="nonzero" fill="#55D4F5" d="M366.085786,819.414214 L336,819.414214 L336,817.414214 L366.085786,817.414214 L359.085786,810.414214 L360.5,809 L369.914214,818.414214 L369.207107,819.12132 L360.5,827.828427 L359.085786,826.414214 L366.085786,819.414214 Z"></path></svg></div><div><span class="cr-tag">'+ offersList[i].offers[index].department +'</span>');
      //     var offer_title = $("<h6><a class='' href="+ offer_url +">"+ offersList[i].offers[index].title +"</a><h6>");
      //     var offer_location = $("<p>"+ offersList[i].offers[index].location +"</p></div></div>");
          
      //     offer.append(offer_department);
      //     offer.append(offer_title);
      //     offer.append(offer_location);
      //     department.append(offer);
      //   }

      // }


    });
  };


  listOffers();

})(jQuery);
