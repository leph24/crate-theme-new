$(document).ready(function(){
  var select_parent = '#parent_selection',
      $select_parent = $(select_parent);

  var select_package = '#selectPackage',
      $select_package = $(select_package);

  var $nightly = $('.js-select-nightly');
  var $testing = $('.js-select-testing');
  var $stable = $('.js-select-stable');

  var parent_value = '';
  var package_value = '';

  var $package_selected = $select_package.val();





  // hide testing and nightly select and select stable
  $testing.hide();
  $nightly.hide();
  $stable.attr('selected', true);

  // show tarball button initially
  $('.js-download').hide();
  $('.js-download-stable').show();





  //If parent option is changed
  $select_parent.change(function() {
      parent_value = $(this).val(); //get option value from parent
      package_value = $select_package.val();

      // hide all options and unselect
      $(select_package + ' option').hide().removeAttr('selected');
      $(select_package + ' option').hide().prop('selected', false);

      if (parent_value == 'testing' || parent_value == 'nightly') {
        $('.js-select-' + parent_value).show().attr('selected',true);
        $('.js-select-' + parent_value).show().prop('selected',true);

        showButton(parent_value);
      } else {
        $(select_package + ' option').show();
        $testing.hide();
        $nightly.hide();

        $stable.attr('selected', true);
        $stable.prop('selected', true);
      }

      showButton(parent_value);

  });





  // CHANGE PACKAGE SELECT
  $($select_package).change(function() {
    var selected = $(this).val();

    showButton(selected);
  });


  // SHOW CORRECT BUTTON FUNCTION
  function showButton(el) {
    $('.js-download').hide();
    $('.js-download-' + el).show();
  };


});