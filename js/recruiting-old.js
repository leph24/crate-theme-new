(function($) {
  var offersList, departments;

  var listOffers = function() {
    $.getJSON("https://api.recruitee.com/c/crateio/careers/offers/?scope=active", function(data) {

      // get departments list
      departments = [];
      for (var i in data.offers) {
        var department = data.offers[i].department;
        if (departments.indexOf(department) > -1) {
          continue;
        }
        departments.push(department);
      }

      offersList = [];

      //restructure offers list
      for (var i in departments) {
        var department = departments[i];
        offersList.push({
          "department": department,
          "offers": data.offers.filter(function(offer) {
            return (offer.department == department && offer.status=="published");
          })
        });

      }

      //render jobs list
      for (var i in offersList){
        var department = $("<div class='cr-department row'>"+"<div class='col-xs-12'><h3 class='cr-department-title'>"+ offersList[i].department +"</h3></div>"+"</div>");
        $("#grnhse_app").append(department);

        for (var index in offersList[i].offers){
          var offer = $("<div class='cr-offer col-xs-12 col-md-6'></div>");
          var offer_url = "https://jobs.crate.io/o/"+offersList[i].offers[index].slug;

          var offer_title = $("<a class='cr-offer-title' href="+ offer_url +">"+ offersList[i].offers[index].title +"</a>");
          var offer_location = $("<div class='cr-offer-location'>"+ offersList[i].offers[index].location +"</div>");
          offer.append(offer_title);
          offer.append(offer_location);
          department.append(offer);
        }
      }


    });
  };


  listOffers();

})(jQuery);
