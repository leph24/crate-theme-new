<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crate
 */

get_header(); 
?>



<?php require_once(locate_template('partials/header.php')); ?>


<div class="cr-section-content">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col">

				<?php if ( have_posts() ) : ?>

					<?php while ( have_posts() ) : the_post(); 

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'template-parts/content', get_post_format() ); ?>

					<?php endwhile; ?>

					<?php
				      if ( function_exists('wp_bootstrap_pagination') )
				        wp_bootstrap_pagination();
				    ?>

				<?php else : ?>
					<!-- NO POSTS -->
					<?php get_template_part( 'template-parts/content', 'none' ); ?>
				<?php endif; ?>

			</div>
		</div>
	</div>
</div>

<?php
get_sidebar();
get_footer();
